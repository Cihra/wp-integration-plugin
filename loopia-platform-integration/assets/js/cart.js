/* cart handler JS */
jQuery( function( $ ) {

	// lgpi_params is required to continue, ensure the object exists
	if ( typeof lgpi_params === 'undefined' ) {
		return false;
	}

	// Utility functions

	/**
	 * Gets a url for a given AJAX endpoint.
	 *
	 * @param {String} endpoint The AJAX Endpoint
	 * @return {String} The URL to use for the request
	 */
	var get_lgpi_url = function( endpoint ) {
		return lgpi_params.lgpi_ajax_url.toString().replace(
			'%%endpoint%%',
			endpoint
		);
	};

	/**
	 * Check if a node is blocked for processing.
	 *
	 * @param {JQuery Object} $node
	 * @return {bool} True if the DOM Element is UI Blocked, false if not.
	 */
	var lgpi_is_blocked = function( $node ) {
		return $node.is( '.processing' ) || $node.parents( '.processing' ).length;
	};

	/**
	 * Block a node visually for processing.
	 *
	 * @param {JQuery Object} $node
	 */
	var lgpi_block = function( $node ) {
		if ( ! lgpi_is_blocked( $node ) ) {
			$node.addClass( 'processing' );
		}
	};

	/**
	 * Unblock a node after processing is complete.
	 *
	 * @param {JQuery Object} $node
	 */
	var lgpi_unblock = function( $node ) {
		$node.removeClass( 'processing' );
	};

	/**
	 * Update the .lgpi_cart_html_container div with a string of html.
	 *
	 * @param {String} html_str The HTML string with which to replace the div
	 */
	var update_lgpi_cart_container = function( html_str ) {
		let $html       = $.parseHTML( html_str );
		let $contents   = $('.lgpi-cart_contents', $html);

		// Check if html includes at minimum content div
		if ( $contents.length !== 0 ) {
			$( '.lgpi_cart_html_inner_container' ).replaceWith( $html );
			// $( document.body ).trigger( 'updated_lgpi_div' );
		}
		return;
	};

	/**
	 * Update the cart button items count
	 *
	 * @param {Int} count item count
	 */
	var update_lgpi_cart_counter = function( count, attention = false ) {
		if ( count.length === 0 ) {
			count = 0;
		}

		var $item_count = count;
		let $targets = $('button.lgpi_cart_toggle_button');

		if ( $targets.length !== 0 ) {
			$targets.each( function(e){
				$( this ).removeClass('lgpi_item_added');
				if ( $item_count == 0 ) {
					$( this ).children('span.lgpi_cart_button_amount_container').replaceWith('<span class="lgpi_cart_button_amount_container"></span>');
				}else{
					$( this ).children('span.lgpi_cart_button_amount_container').replaceWith('<span class="lgpi_cart_button_amount_container"><span>'+$item_count+'</span></span>');
					if (attention) {
						$( this ).addClass('lgpi_item_added');
					}
				}
			});
		}
		return;
	};

	/**
	 * Update the .lgpi-notices-wrapper div with a string of html.
	 *
	 * @param {String} html_str The HTML string with which to replace the div
	 */
	var update_lgpi_cart_notice = function( html_str ) {
		var $html       = $.parseHTML( html_str );
		var $contents   = $('.lgpi-notices-wrapper', $html);
		if ( $contents.length !== 0 ) {
			$( '.lgpi-notices-wrapper' ).replaceWith( $contents );
			// $( '.lgpi-notices-wrapper' ).delay(3000).addClass('.lgpi_fadeout');
			lgpi_fadeout( $( '.lgpi-notices-wrapper'), 3000 );

		}
		return;
	};

	/**
	 * FadeIn a node visually
	 *
	 * @param {JQuery Object} $node
	 *
	 */
	 var lgpi_fadein = function( $node ) {
	 	if ( $node.is( '.lgpi_fadeout' ) ) {
	 		$node.removeClass('lgpi_fadeout');
	 	}
	 };

	 /**
	  * FadeOut a node visually
	  *
	  * @param {JQuery Object} $node
	  *
	  */
	var lgpi_fadeout = function( $node, $timer ) {
		if ( ! $node.is( '.lgpi_fadeout' ) ) {
			if ($timer){
				setTimeout( function(){ $node.addClass('lgpi_fadeout') }, $timer);
			} else {
				$node.addClass('lgpi_fadeout');
			}
		}
	};

	/**
	 * Object to handle cart
	 */
	var lgpi_cart = {
		/**
		 * Initialize cart events
		 */
		init: function() {
			this.lgpi_update_cart           = this.lgpi_update_cart.bind( this );
			this.lgpi_redirect_to_orderflow = this.lgpi_redirect_to_orderflow.bind( this );
			this.lgpi_add_to_cart          	= this.lgpi_add_to_cart.bind( this );
			this.lgpi_toggle_show_cart      = this.lgpi_toggle_show_cart.bind( this );

			$( document ).on(
				'lgpi_update_cart',
				function() { lgpi_cart.lgpi_update_cart(); } );
			$( document ).on(
				'lgpi_redirect_to_orderflow',
				function() { lgpi_cart.lgpi_redirect_to_orderflow(); } );
			$( document ).on(
				'lgpi_update_cart_item_counter',
				function() { lgpi_cart.lgpi_update_cart_item_counter(); } );
			$( document ).on(
				'lgpi_check_login',
				function() { lgpi_cart.lgpi_check_login(); } );
			$( document ).on(
				'click',
				'.lgpi_cart_html_inner_container button.lgpi_update_cart_button',
				this.lgpi_update_click );
			$( document ).on(
				'click',
				'.lgpi_add_to_cart',
				this.lgpi_add_to_cart );
			$( document ).on(
				'click',
				'.lgpi_cart_toggle_button',
				this.lgpi_toggle_show_cart );
			$( document ).on(
				'click',
				'.lgpi_close_shopping_cart_button, .lgpi_cart_html_overlay',
				this.lgpi_close_cart );
			$( document ).on(
				'click',
				'.lgpi_cart_item_remove_button',
				this.lgpi_remove_from_cart );
			$( document ).on(
				'click',
				'.lgpi_logout_button_link',
				this.lgpi_logout_click );
			$( document ).on(
				'click',
				'.lgpi_trigger_dropdown',
				this.lgpi_account_dropdown_trigger_toggle );
			$( document ).on(
				'mouseenter mouseleave',
				'.lgpi_trigger_dropdown_hover',
				this.lgpi_account_dropdown_trigger_toggle );

			// Dont sync the cart automatically for now
			// $( document.body ).trigger( 'lgpi_update_cart' );
			// check login on load
			$( document.body ).trigger( 'lgpi_check_login' );
			$( document.body ).trigger( 'lgpi_update_cart_item_counter' );
		},

		/**
		 * Redirect user to orderflow!
		 */
		lgpi_redirect_to_orderflow: function( items = null ) {
			var data = {};
			if (items) {
				data[ 'new-items' ] = items;
			}
			// Make call to WPAJAX
			$.ajax( {
				type:     'POST',
				url:      get_lgpi_url( 'lgpi_orderflow' ),
				dataType: 'json',
				data: data,
				success:  function( response ) {
					if ( response['url'].length ) {
						window.location.href = response['url'];
					}else{
						// do nothing.. 
					}
				}
			} );
		},

		/**
		 * Update entire cart via ajax
		 */
		lgpi_update_cart: function(  ) {
			var $inner_container = $( '.lgpi_cart_html_inner_container' );
			// for updating the first time, add class for indication

			// make sure cart update is not already triggered
			if ( !lgpi_is_blocked($inner_container) ){
				lgpi_block( $inner_container );
				// Make call to API
				$.ajax( {
					type:     'POST',
					url:      get_lgpi_url( 'lgpi_update_cart' ),
					dataType: 'html',
					success:  function( response ) {
						update_lgpi_cart_container( response );
					},
					complete: function() {
						lgpi_unblock( $inner_container );
						$( '.lgpi_cart_html_container' ).addClass('lgpi_cart_synced');
						// fadeout notice after 3seconds
						lgpi_fadeout( $( '.lgpi-notices-wrapper'), 3000 );
						// $.scroll_to_notices( $( '[role="alert"]' ) );
						$( document.body ).trigger( 'lgpi_update_cart_item_counter' );
						// Check VAT status for updated CART items
						check_vat_status();
					}
				} );
			}else{
				// console.log('Cart is already loading..');
			}
		},

		/**
		 * Update entire cart via ajax
		 */
		lgpi_update_cart_item_counter: function(  ) {
			// get counter
			$.ajax( {
				type:     'POST',
				url:      get_lgpi_url( 'lgpi_get_cart_item_count' ),
				dataType: 'json',
				success:  function( response ) {
					if (response['items']) {
						update_lgpi_cart_counter(response['items']);
					}else{
						update_lgpi_cart_counter(0);
					}
				}
			} );
		},

		/**
		 * Handling for update_cart button
		 *
		 */
		lgpi_update_click: function( evt ) {
			evt.preventDefault();
			$( document.body ).trigger( 'lgpi_update_cart' );
		},


		/**
		 * Handling for logout button
		 *
		 */
		lgpi_logout_click: function( evt ) {
			evt.preventDefault();
			var $thisbutton = $(evt.currentTarget);
			// console.log('Logging out..');

			if ( $thisbutton.is( '.lgpi_loading' ) ) {
				// if the last click is still processing, dont do anything yet..
				// console.log('Click spam rejection');
				return true;
			}

			$thisbutton.addClass( 'lgpi_loading' );
			lgpi_block($thisbutton);

			// Make call to API
			$.ajax( {
				type:     'POST',
				url:      get_lgpi_url( 'lgpi_user_logout' ),
				dataType: 'html',
				success:  function( response ) {
				},
				complete: function() {
					// Reload page after logging out
					location.reload();

				}
			} );
		},


		/**
		 * Check login status from API
		 *
		 */
		lgpi_check_login: function( evt ) {
			// Make call to API
			$.ajax( {
				type:     'POST',
				url:      get_lgpi_url( 'lgpi_user_details' ),
				dataType: 'json',
				success:  function( response ) {
					let data = response;
					if ( data['logged-in'] == "true" && data['user-data']['username'].length ) {
						let username = data['user-data']['username'];
						$('.lgpi_login_button_link').hide();
						$('.lgpi_login_dropdown_container').addClass('lgpi_trigger_dropdown_hover');
						$('.lgpi_open_dropdown_content ').addClass('lgpi_trigger_dropdown').fadeIn( 500 );
						$('.lgpi_logout_button_link ').fadeIn( 500 );
						let admin_html = username;
						if ( data['user-data']['url-admin'].length && data['user-data']['url-admin'] !== false ){ 
							admin_html = '<a href="'+data['user-data']['url-admin']+'">'+username+'</a>';
						}
						$('.lgpi_login_username').html(admin_html).fadeIn(500);
						if ( data['user-data']['email'].length ){ 
							$('.lgpi_login_email').html(data['user-data']['email']).fadeIn(500);
						}
						if ( data['user-data']['url-services'].length && data['user-data']['url-services'] !== false ){ 
							let services_html = '<a href="'+data['user-data']['url-services']+'">'+data['user-data']['name-services']+'</a>';
							$('.lgpi_login_services').html(services_html).fadeIn(500); 
						}
						if ( data['user-data']['url-invoices'].length && data['user-data']['url-invoices'] !== false ){ 
							let invoices_html = '<a href="'+data['user-data']['url-invoices']+'">'+data['user-data']['name-invoices']+'</a>';
							$('.lgpi_login_invoices').html(invoices_html).fadeIn(500); 
						}
						if ( data['user-data']['url-account'].length && data['user-data']['url-account'] !== false ){ 
							let account_html = '<a href="'+data['user-data']['url-account']+'">'+data['user-data']['name-account']+'</a>';
							$('.lgpi_account').html(account_html).fadeIn(500); 
						}
					}else{
						$('.lgpi_logout_button_link ').hide();
						$('.lgpi_login_username').html("").fadeOut(500);
						$('.lgpi_login_email').html("").fadeOut(500);
						$('.lgpi_login_button_link').fadeIn( 500 );
					}
				},
				complete: function() {
					lgpi_unblock( $('.lgpi_user_details_container') );
					// console.log('Login check complete');
				}
			} );
		},

		/**
		 * Handler for login account dropdown
		 *
		 */
		lgpi_account_dropdown_trigger_toggle: function( evt ) {
			evt.preventDefault();
			var $thisbutton = $(evt.currentTarget);
			var $target = $thisbutton.parent().find('.lgpi_dropdown_content');
			if ( $target.length ) {
				if ( $target.hasClass('open') ) {
					$thisbutton.attr('aria-expanded', 'false');
					$target.fadeOut(300).removeClass('open').attr('aria-expanded', 'false');
				}else{
					$thisbutton.attr('aria-expanded', 'true');
					$target.fadeIn(300).addClass('open').attr('aria-expanded', 'true');
				}
			}
			return false;
		},


		/**
		 * Add to cart button handler
		 *
		 */
		lgpi_add_to_cart: function( evt ) {

			var $thisbutton = $(evt.currentTarget);
			var data = {};

			if ( $thisbutton.is( '.lgpi_ajax_add_to_cart' ) ) {

				// event prevented for AJAX
				evt.preventDefault();

				if ( $thisbutton.is( '.lgpi_loading' ) ) {
					// if the last click is still processing, dont do anything yet..
					// console.log('Waiting for last request..');
					return true;
				}
				if ( ! $thisbutton.attr( 'data-product_code' ) ) {
					// add_to_cart button is missing a product code
					// console.log('Missing product code');
					return true;
				}

				$thisbutton.addClass( 'lgpi_loading' ).removeClass('lgpi_done').removeClass('lgpi_failed');
				lgpi_block($thisbutton);
				// $thisbutton.addClass( 'lgpi_loading' ).prop( 'disabled', true );

				// jquery data store
				$.each( $thisbutton.data(), function( key, value ) {
					data[ key ] = value;
				});

				// Fetch data attributes
				$.each( $thisbutton[0].dataset, function( key, value ) {
					data[ key ] = value;
				});

				// Make call the lgpi_add_to_cart handler
				$.ajax( {
					type:     'POST',
					url:      get_lgpi_url( 'lgpi_add_to_cart' ),
					data:     data,
					dataType: 'json',
					success:  function( response ) {
					},
					complete: function( response ) {
						let data = response.responseJSON;
						let item_count = 0;
						if ( typeof data['items'] !== 'undefined' ) {
							item_count = data['items'];
						}
						update_lgpi_cart_counter(item_count, true);
						// remove old indicators
						$('.lgpi_add_to_cart').removeClass('lgpi_done').removeClass('lgpi_failed');
						if ( data['added-to-cart'] == "true" ) {
							$thisbutton.removeClass( 'lgpi_loading' ).removeClass( 'lgpi_ga_done' ).addClass('lgpi_done');
							$( document.body ).trigger( 'lgpi_update_cart' );
							$( document.body ).trigger( 'lgpi_added_to_cart' );
							if ( data['new-items'] ) {
								lgpi_cart.lgpi_redirect_to_orderflow(data['new-items']);
							}
						}else{
							$thisbutton.removeClass( 'lgpi_loading' ).addClass('lgpi_failed');
						}
						lgpi_unblock($thisbutton);
						// $thisbutton.removeClass( 'lgpi_loading' ).prop( 'disabled', false );
					}
				} );
			}else{
				// console.log('Not ajax request');
			}
		},


		/**
		 * Remove from cart button handler
		 *
		 */
		lgpi_remove_from_cart: function( evt ) {

			var $thisbutton = $(evt.currentTarget);
			var data = {};

			if ( $thisbutton.is( '.lgpi_ajax_remove_from_cart' ) ) {
				if ( $thisbutton.is( '.lgpi_loading' ) ) {
					// if the last click is still processing, dont do anything yet..
					// console.log('Waiting for last request..');
					return true;
				}
				if ( ! $thisbutton.attr( 'data-product_code' ) ) {
					// add_to_cart button is missing a product code
					// console.log('Missing product code');
					return true;
				}

				// event prevented for AJAX
				evt.preventDefault();
				lgpi_block( $thisbutton.parent( '.lgpi-cart-item' ) );
				$thisbutton.addClass( 'lgpi_loading' ).removeClass('lgpi_done').removeClass('lgpi_failed').prop( 'disabled', true );

				// jquery data store
				$.each( $thisbutton.data(), function( key, value ) {
					data[ key ] = value;
				});

				// Fetch data attributes
				$.each( $thisbutton[0].dataset, function( key, value ) {
					data[ key ] = value;
				});

				// Make call the lgpi_remove_from_cart handler
				$.ajax( {
					type:     'POST',
					url:      get_lgpi_url( 'lgpi_remove_from_cart' ),
					data:     data,
					dataType: 'json',
					success:  function( response ) {
					},
					complete: function( response ) {
						let data = response.responseJSON;
						let item_count = 0;
						if ( typeof data['items'] !== 'undefined' ) {
							item_count = data['items'];
						}
						update_lgpi_cart_counter(item_count);
						if ( data['removed-from-cart'] == "true" ) {
							$thisbutton.removeClass( 'lgpi_loading' ).addClass('lgpi_done').prop( 'disabled', false );
							$thisbutton.parent( '.lgpi-cart-item' ).replaceWith('<div class="deleted"></div>');
							$( document.body ).trigger( 'lgpi_update_cart' );
						}else{
							$thisbutton.removeClass( 'lgpi_loading' ).addClass('lgpi_failed').prop( 'disabled', false );
						}
					}
				} );
			}else{
				// console.log('Not ajax request');
			}
		},

		/**
		 * Show cart func
		 *
		 */
		lgpi_toggle_show_cart: function( evt ) {

			var $checkout = $( '#lgpi_checkout_container' );
			var $html_container = $( '.lgpi_cart_html_container' );
			var $thisbutton = $(evt.currentTarget);

			// if we are on SPA-page
			if ( $checkout.length ) {
				// Dont open cart,show the checkout instead -> scroll to element
				// $([document.documentElement, document.body]).animate({
				//     scrollTop: ($checkout.offset().top - 20)
				// }, 500);
				// return false;
			}
			if ( $html_container.length ) {
				if ( !$html_container.hasClass('lgpi_cart_synced') || $checkout.length ) {
					// Sync the cart when cart is opened for the first time, OR in SPA
					$( document.body ).trigger( 'lgpi_update_cart' );
				}

				if ($html_container.hasClass('lgpi_cart_visible')) {
					// Close if open
					$('.lgpi_cart_toggle_button.lgpi-open-toggled').attr('aria-expanded', 'false').removeClass('lgpi-open-toggled');
					$html_container.fadeOut(500).removeClass('lgpi_cart_visible').attr('aria-expanded', 'false');
				} else {
					// Open if closed
					$thisbutton.attr('aria-expanded', 'true').addClass('lgpi-open-toggled');
					$html_container.fadeIn(500).addClass('lgpi_cart_visible').attr('aria-expanded', 'true');
					// Focus for accessability by keyboard
					$html_container.find('.lgpi_close_shopping_cart_button').focus();
				}
			}
		},

		/**
		 * Close cart func
		 *
		 */
		lgpi_close_cart: function( evt ) {

			var $html_container = $( '.lgpi_cart_html_container' );

			if ( $html_container.hasClass('lgpi_cart_visible') ) {
				// Close if open
				$('.lgpi_cart_toggle_button.lgpi-open-toggled').attr('aria-expanded', 'false').removeClass('lgpi-open-toggled').focus(); // Focus for accessability by keyboard
				$html_container.fadeOut(500).removeClass('lgpi_cart_visible').attr('aria-expanded', 'false');
			}
		},
	};

	lgpi_cart.init();

	/**
	 * change VAT toggle status
	 *
	 */
	var change_vat_status = function() {

		let $targets = $('.lgpi_price_inner_container');
		let $vat_changers = $('.lgpi_vat_toggle_inner_container');

		let $lgpi_vat_status = "excluded";

		if (typeof(Storage) !== "undefined") {
			if ( localStorage.getItem('lgpi_vat_status') == 'included' ) {
				localStorage.setItem('lgpi_vat_status', 'excluded');
				$lgpi_vat_status = "excluded";
			}else{
				localStorage.setItem('lgpi_vat_status', 'included');
				$lgpi_vat_status = "included";
			}
		}

		if ( $vat_changers.length !== 0 ) {
			$vat_changers.each( function(e){
				if ( $lgpi_vat_status == "included" ) {
					$( this ).addClass('lgpi_status_vat_included');
				}else{
					$( this ).removeClass('lgpi_status_vat_included');
				}
			});
		}

		if ( $targets.length !== 0 ) {
			$targets.each( function(e){
				if ( $lgpi_vat_status == "included" ) {
					$( this ).addClass('lgpi_status_vat_included');
				}else{
					$( this ).removeClass('lgpi_status_vat_included');
				}
			});
		}
		return;
	};

	/**
	 * check VAT toggle status
	 *
	 */
	var check_vat_status = function() {

		let $targets = $('.lgpi_price_inner_container');
		let $vat_changers = $('.lgpi_vat_toggle_inner_container');

		let $lgpi_vat_status = "excluded";

		if (typeof(Storage) !== "undefined") {
			if ( localStorage.getItem('lgpi_vat_status') ) {
				$lgpi_vat_status = localStorage.getItem('lgpi_vat_status');
			}
		}

		if ( $vat_changers.length !== 0 ) {
			$vat_changers.each( function(e){
				if ( $lgpi_vat_status == "included" ) {
					$( this ).addClass('lgpi_status_vat_included');
				}else{
					$( this ).removeClass('lgpi_status_vat_included');
				}
			});
		}

		if ( $targets.length !== 0 ) {
			$targets.each( function(e){
				if ( $lgpi_vat_status == "included" ) {
					$( this ).addClass('lgpi_status_vat_included');
				}else{
					$( this ).removeClass('lgpi_status_vat_included');
				}
			});
		}
		return;
	};

	// initial vat status check
	check_vat_status();

	// bind event listener
	$('button.toggle_vat_status').on('click', function(e){
		change_vat_status();
	});

	// workaround for SPA CSS order
	var app_css = $('#lgpi_checkout_app-css');
	var child_style_css = $('#child-styles-css');
	if (app_css.length && child_style_css.length) {
		// console.log('Change order');
		app_css.insertAfter(child_style_css);
	}

} );



/**
 * Update the cart button items count
 *
 * @param {Int} count item count
 */
var update_lgpi_cart_counter = function( count, attention = false ) {
	if ( count.length === 0 ) {
		count = 0;
	}

	var $item_count = count;
	let $targets = document.querySelectorAll('button.lgpi_cart_toggle_button');

	document.cookie = "lgpi_cart_amount="+$item_count+"; expires=session; path=/"

	if ( $targets.length !== 0 ) {
		$targets.forEach( function(e){
			e.classList.remove("lgpi_item_added");
			if ( $item_count == 0 ) {
				e.querySelectorAll('span.lgpi_cart_button_amount_container')[0].innerHTML = '';
			}else{
				e.querySelectorAll('span.lgpi_cart_button_amount_container')[0].innerHTML = '<span>'+$item_count+'</span>';
				if (attention) {
					e.classList.add("lgpi_item_added");
				}
			}
		});
	}
	return;
};
document.addEventListener('DOMContentLoaded', function() {
	if ( typeof __WS_New_Order__ !== 'undefined' ) {
	    __WS_New_Order__.hooks.cartItemsCount.subscribe(function(cartItemsCount) {
	    	update_lgpi_cart_counter(cartItemsCount, true);
	    });
	}
});