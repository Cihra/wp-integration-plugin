<?php
/**
 * Setup menus in WP admin.
 *
 */

defined( 'ABSPATH' ) || exit;


/**
 * LGPI_Admin_Menus Class.
 */
class LGPI_Admin_Menus {

	public function __construct() {
		// admin menu link
		add_action( 'admin_menu', array( $this, 'admin_menu' ), 15 );
	}

	/**
	 * Add menu items.
	 */
	public function admin_menu() {
		add_menu_page( __('Loopia settings', 'LGPI'), 		__('Loopia settings', 'LGPI'), 		'administrator', 		'loopia-settings', array( $this, 'settings_page' ) , '', 99 );
	}


	/**
	 * Settings page HTML
	 */
	public function settings_page() {
		include_once LGPI_ABSPATH . 'includes/html-settings.php';
	}
}

return new LGPI_Admin_Menus();