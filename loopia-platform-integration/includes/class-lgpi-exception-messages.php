<?php

defined( 'ABSPATH' ) || exit;

class LgpiExceptionMessages
{
    const HEADERS_NOT_SET = "Headers are not set";
    const MISSING_ITEM_ID = 'Missing item id';
    const UNEXPECTED_RESPONSE = 'Unexpected response';
    const API_CONNECTION_ERROR = 'API connection error';
}
