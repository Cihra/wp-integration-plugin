<?php
/**
 * Setup post types and metaboxes
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * LGPI_post_type_setup Class.
 *
 *
 **post_type: lgpi-product
 *	price 			(excld VAT)
 *	price_vat 		(incld VAT)
 *	sale_price 		(excld VAT)
 *	sale_price_vat 	(incld VAT)
 *	sale_start 		(date)
 *	sale_end 		(date)
 *	note 			(vat%)
 *	product_data(?)
 *	source 			(frm source?)
 *	id 				(frm id?)
 **
 *
 *
 *
 */
class LGPI_post_type_setup {

	public function __construct() {
		add_action( 'init', array( $this, 'register_post_types' ), 10 );
		add_action( 'save_post', array( $this, 'lgpi_save_products_meta' ), 1, 2 );
		add_filter( 'manage_edit-lgpi-product_columns', array( $this, 'manage_edit_admin_columns' ), 10, 1 );
		add_action( 'manage_posts_custom_column', array($this, 'add_custom_column_data'), 10, 2 );
	}

	/**
	 * register post types
	 */
	public function register_post_types(): void
	{

		// This is the machine name of the content type
		$content_type_name = __('Product data', 'LGPI');
		$content_type_name_plural = __('Product data', 'LGPI');
		$content_type_slug = 'lgpi-product';
		// $content_type_slug_rewrite = 'lgpi-product';

		register_post_type(
			// Label 
			$content_type_slug,
			array(
				"labels" => array(
				    'name' 					=> $content_type_name_plural,
				    'singular_name' 		=> $content_type_name,
					'menu_name' 			=> $content_type_name_plural,
					'add_new'               => __('Add new', 'LGPI'),
					'add_new_item'          => __('Add new', 'LGPI').' '.$content_type_name,
					'new_item'              => __('New', 'LGPI').' '.$content_type_name,
					'edit_item'             => __('Edit', 'LGPI'),
					'view_item'             => __('Show', 'LGPI').' '.$content_type_name,
					'all_items'             => __('All', 'LGPI').' '.$content_type_name_plural,
					'search_items'          => __('Find', 'LGPI').' '.$content_type_name_plural,
					'parent_item_colon'     => __('Older', 'LGPI').' '.$content_type_name_plural,
					'not_found'             => __('None was found', 'LGPI'),
					'not_found_in_trash'    => __('None was found in trashbin', 'LGPI')
				),
				// "description" => "LGPI-product pricelist",
				"public" => false,
				"hierarchical" => false,
				"exclude_from_search" => true,
				"publicly_queryable" => false,
				"show_ui" => true,
				"show_in_menu" => true,
				"menu_position" => 40,
				"show_in_nav_menus" => false,
				"show_in_admin_bar" => false,
				"show_in_rest" => false,
			    'menu_icon'  => 'dashicons-database',
			    'supports' => array( 'title' ),
			    'has_archive' => false,
			    // 'rewrite'    => array( 'slug' => $content_type_slug_rewrite ),
			    'query_var' => false,
			    'delete_with_user' => false,
			    'register_meta_box_cb' => array($this, 'register_product_data_metaboxes'),
			)
		);
	}

	/**
	 * register metaboxes for product data
	 */
	public function register_product_data_metaboxes(): void
	{
		add_meta_box(
			'lgpi_product_data_fields',
			'Product data',
			array($this, 'lgpi_product_data_fields'),
			'lgpi-product',
			'normal',
			'high'
		);
	}
	
	public function manage_edit_admin_columns( $columns ){
	    $columns['meta-lgpi_price_vat_excluded'] = 'Price';
	    $columns['meta-lgpi_sale_price_vat_excluded'] = 'Campaign price';
	    return $columns;
	}

	function add_custom_column_data( $column_name, $post_id ){
	    switch ( $column_name ) {
	        case 'meta-lgpi_price_vat_excluded':
	            echo get_post_meta( $post_id, 'lgpi_price_vat_excluded', true );
	            break;
	        case 'meta-lgpi_sale_price_vat_excluded':
	            echo get_post_meta( $post_id, 'lgpi_sale_price_vat_excluded', true );
	            break;
	        default:
	            break;
	    }
	}

	/**
	 * output metabox HTML
	 */
	public function lgpi_product_data_fields(){
		global $post;

		// Nonce field to validate form request came from current site
		wp_nonce_field( basename( __FILE__ ), 'lgpi_product_data_fields' );

		$lgpi_price_vat_excluded = get_post_meta( $post->ID, 'lgpi_price_vat_excluded', true );
		$lgpi_sale_price_vat_excluded = get_post_meta( $post->ID, 'lgpi_sale_price_vat_excluded', true );
		// $lgpi_price_vat_note = get_post_meta( $post->ID, 'lgpi_price_vat_note', true );

		// display shortcode
		echo '<div>';
		echo '<label for="lgpi_shortcode_display" style="display:inline-block;width:250px;">Shortcode to display price</label>  ';
		echo '<input type="text" id="lgpi_shortcode_display" name="lgpi_shortcode_display" readonly="readonly" value="[lgpi-price id='  . $post->ID . ']" > or with prefix/suffix <input type="text" id="lgpi_shortcode_display_pre_suf" name="lgpi_shortcode_display_pre_suf" readonly="readonly" style="width:350px;" value=\'[lgpi-price id='  . $post->ID . ' prefix="from" suffix="/month"]\' >';
		echo '</div>';

		echo '<div>';
		echo '<label for="lgpi_price_vat_excluded" style="display:inline-block;width:250px;">Price (VAT excluded)</label>  ';
		echo '<input type="text" id="lgpi_price_vat_excluded" name="lgpi_price_vat_excluded" value="' . esc_textarea( $lgpi_price_vat_excluded )  . '" class="">';
		echo '</div>';

		echo '<div>';
		echo '<label for="lgpi_sale_price_vat_excluded" style="display:inline-block;width:250px;">Sale/campaign price (VAT excluded)</label>  ';
		echo '<input type="text" id="lgpi_sale_price_vat_excluded" name="lgpi_sale_price_vat_excluded" value="' . esc_textarea( $lgpi_sale_price_vat_excluded )  . '" class="">';
		echo '</div>';

		// echo '<div>';
		// echo '<label for="lgpi_price_vat_note" style="display:inline-block;width:250px;">Note/suffix for price</label>  ';
		// echo '<input type="text" id="lgpi_price_vat_note" name="lgpi_price_vat_note" value="' . esc_textarea( $lgpi_price_vat_note )  . '" class="">';
		// echo '</div>';

	}

	/**
	 * Save the metabox data
	 */
	public function lgpi_save_products_meta( $post_id, $post ) {
		// Return if the user doesn't have edit permissions
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		// Verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times
		if ( 
			! isset( $_POST['lgpi_price_vat_excluded'] ) || 
			! isset( $_POST['lgpi_sale_price_vat_excluded'] ) || 
			! wp_verify_nonce( $_POST['lgpi_product_data_fields'], basename(__FILE__) ) ) {
			return $post_id;
		}

		$lgpi_price_vat_excluded_value = false;
		$lgpi_sale_price_vat_excluded_value = false;

		// add separation between "empty" == not set and "0" == free
		if ( $_POST['lgpi_price_vat_excluded']  !== '' ) {
			$lgpi_price_vat_excluded_value = floatval(esc_textarea( $_POST['lgpi_price_vat_excluded']));
		}
		if ( $_POST['lgpi_sale_price_vat_excluded'] !== '' ) {
			$lgpi_sale_price_vat_excluded_value = floatval(esc_textarea( $_POST['lgpi_sale_price_vat_excluded']));
		}

		// Now that we're authenticated, time to save the data
		$products_meta['lgpi_price_vat_excluded'] = $lgpi_price_vat_excluded_value;
		$products_meta['lgpi_sale_price_vat_excluded'] = $lgpi_sale_price_vat_excluded_value;
		// $products_meta['lgpi_price_vat_note'] = esc_textarea( $_POST['lgpi_price_vat_note'] );

		// Cycle through the $products_meta array and update/create/delete meta keys accordingly
		foreach ( $products_meta as $key => $value ){
			if ( 'revision' === $post->post_type ) {
				// Don't store custom data twice
				return;
			}
			if ( get_post_meta( $post_id, $key, false ) ) {
				// If the custom field already has a value, update it
				update_post_meta( $post_id, $key, $value );
			} else {
				// If the custom field doesn't have a value, add it
				add_post_meta( $post_id, $key, $value);
			}
			if ( $value === false ) {
				// Delete the meta key if there's no value
				delete_post_meta( $post_id, $key );
			}
		}
	}
}

return new LGPI_post_type_setup();