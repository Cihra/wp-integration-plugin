<?php

defined( 'ABSPATH' ) || exit;

class LgpiSetting
{
    protected $type;

    protected $save_as_options;

    protected $title;

    protected $value;

    protected $text;

    /**
     * LgpiSettings constructor.
     * @param string $title
     * @param string $type
     * @param bool $save_as_options
     * @param string $value
     */
    public function __construct($title, $type, $save_as_options, $value, $text = '')
    {
        $this->title = $title;
        $this->type = $type;
        $this->value = $value;
        $this->save_as_options = $save_as_options;
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getSaveAsOptions()
    {
        return $this->save_as_options;
    }

    /**
     * @param mixed $save_as_options
     */
    public function setSaveAsOptions($save_as_options)
    {
        $this->save_as_options = $save_as_options;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}
