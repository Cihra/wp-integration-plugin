<?php

defined( 'ABSPATH' ) || exit;

class LgpiSettingsCache
{
    /**
     * @var LgpiSetting
     */
    protected $setting;

    /**
     * @var array
     */
    protected $settings_data = [];

    /**
     * @return array
     */
    public function getSettingsData(): array
    {
        return $this->settings_data;
    }

    /**
     * @param array $settings_data
     */
    public function setSettingsData(array $settings_data): void
    {
        $this->settings_data = $settings_data;
    }

    public function __construct()
    {

    }

    public function get_checkout_settings()
    {
        $checkout_parameters = [];

        // Check required settings for checkout!
        // "webUrl" = homepage url
        $lgpi_checkout_web_url = home_url();
        $lgpi_checkout_api_url = $this->settings_data['lgpi_checkout_api_url']->getValue();
        $lgpi_checkout_base_url = $this->settings_data[ 'lgpi_checkout_base_url' ]->getValue();
        $lgpi_checkout_asset_url = $this->settings_data[ 'lgpi_checkout_asset_url' ]->getValue();
        $lgpi_checkout_login_url = $this->settings_data[ 'lgpi_checkout_login_url' ]->getValue();
        $lgpi_checkout_logout_url = $this->settings_data[ 'lgpi_checkout_logout_url' ]->getValue();
        $lgpi_checkout_knowledgebase_url = $this->settings_data[ 'lgpi_checkout_knowledgebase_url' ]->getValue();
        // $lgpi_checkout_helpdesk_url = $this->settings_data[ 'lgpi_checkout_helpdesk_url' ]->getValue();
        $lgpi_checkout_webadmin_url = $this->settings_data[ 'lgpi_checkout_webadmin_url' ]->getValue();
        $lgpi_checkout_webadmin_base_invoice_url = $this->settings_data[ 'lgpi_checkout_webadmin_base_invoice_url' ]->getValue();
        $lgpi_checkout_company = $this->settings_data[ 'lgpi_checkout_company' ]->getValue();

        // json encoded strings?
        $lgpi_checkout_languages = json_decode( $this->settings_data[ 'lgpi_checkout_languages' ]->getValue(), true );
        // This (tosUrls) is now deprecated, use tosUrl instead.
        // $lgpi_checkout_tos_urls = json_decode( $this->settings_data[ 'lgpi_checkout_tos_urls' ]->getValue(), true );

        // optional params
        $lgpi_checkout_currency = $this->settings_data[ 'lgpi_checkout_currency' ]->getValue();
        $lgpi_checkout_company_country = $this->settings_data[ 'lgpi_checkout_company_country' ]->getValue();
        $lgpi_checkout_gtag = $this->settings_data[ 'lgpi_checkout_gtag' ]->getValue();
        $lgpi_checkout_workflow = $this->settings_data[ 'lgpi_checkout_workflow' ]->getValue();
        $lgpi_checkout_env = $this->settings_data[ 'lgpi_checkout_env' ]->getValue();
        $lgpi_checkout_tos_url = $this->settings_data[ 'lgpi_checkout_tos_url' ]->getValue();
        $lgpi_checkout_hostings_url = $this->settings_data[ 'lgpi_checkout_hostings_url' ]->getValue();
        $lgpi_checkout_migrationinfo_url = $this->settings_data[ 'lgpi_checkout_migrationinfo_url' ]->getValue();
        $lgpi_checkout_transfer_url = $this->settings_data[ 'lgpi_checkout_transfer_url' ]->getValue();
        $lgpi_checkout_theme = $this->settings_data[ 'lgpi_checkout_theme' ]->getValue();

        if (
            $lgpi_checkout_web_url &&
            $lgpi_checkout_api_url &&
            $lgpi_checkout_base_url &&
            $lgpi_checkout_asset_url &&
            $lgpi_checkout_login_url &&
            $lgpi_checkout_logout_url &&
            $lgpi_checkout_knowledgebase_url &&
            $lgpi_checkout_webadmin_url &&
            $lgpi_checkout_webadmin_base_invoice_url &&
            $lgpi_checkout_company &&
            $lgpi_checkout_languages
        ) {
            // ALL of the required fields must be set
            $checkout_parameters = array(
                "webUrl" => $lgpi_checkout_web_url,
                "apiUrl" => $lgpi_checkout_api_url,
                "baseUrl" => $lgpi_checkout_base_url,
                "assetUrl" => $lgpi_checkout_asset_url,
                "loginUrl" => __($lgpi_checkout_login_url, 'LGPI'),
                "logoutUrl" => $lgpi_checkout_logout_url,
                "supportUrl" => __($lgpi_checkout_knowledgebase_url, 'LGPI'),
                "webAdminUrl" => __($lgpi_checkout_webadmin_url, 'LGPI'),
                "baseInvoiceUrl" => __($lgpi_checkout_webadmin_base_invoice_url, 'LGPI'),
                "company" => $lgpi_checkout_company,
                "languages" => $lgpi_checkout_languages
            );
        }

        // check and set Optional parameters
        if ( $checkout_parameters && $lgpi_checkout_company_country) { $checkout_parameters["companyCountry"] = $lgpi_checkout_company_country; }
        if ( $checkout_parameters && $lgpi_checkout_gtag) { $checkout_parameters["gtag"] = $lgpi_checkout_gtag; }
        if ( $checkout_parameters && $lgpi_checkout_currency) { $checkout_parameters["currency"] = $lgpi_checkout_currency; }
        if ( $checkout_parameters && $lgpi_checkout_workflow) { $checkout_parameters["workflow"] = $lgpi_checkout_workflow; }
        if ( $checkout_parameters && $lgpi_checkout_env) { $checkout_parameters["env"] = $lgpi_checkout_env; }
        if ( $checkout_parameters && $lgpi_checkout_tos_url) { $checkout_parameters["tosUrl"] = __($lgpi_checkout_tos_url, 'LGPI'); }
        if ( $checkout_parameters && $lgpi_checkout_hostings_url) { $checkout_parameters["hostingsUrl"] = __($lgpi_checkout_hostings_url, 'LGPI'); }
        if ( $checkout_parameters && $lgpi_checkout_migrationinfo_url) { $checkout_parameters["migrationInfoUrl"] = __($lgpi_checkout_migrationinfo_url, 'LGPI'); }
        if ( $checkout_parameters && $lgpi_checkout_transfer_url) { $checkout_parameters["transferUrl"] = __($lgpi_checkout_transfer_url, 'LGPI'); }
        if ( $checkout_parameters && $lgpi_checkout_theme) { $checkout_parameters["theme"] = $lgpi_checkout_theme; }
        // if ( $checkout_parameters && $lgpi_checkout_tos_urls) { $checkout_parameters["tosUrls"] = $lgpi_checkout_tos_urls; }
        return $checkout_parameters;
    }

    /**
     * get cart/checkout URL
     *
     */
    public function get_checkout_url() {
        return trim($this->settings_data['lgpi_internal_checkout_page_url']->getValue());
    }
}
