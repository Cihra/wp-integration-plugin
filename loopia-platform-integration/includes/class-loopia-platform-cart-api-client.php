<?php

defined( 'ABSPATH' ) || exit;

require_once 'class-loopia-platform-cookie-crumbler.php';
require_once 'class-lgpi-exception-messages.php';

use GuzzleHttp\Client;

class LoopiaPlatformCartApiClient
{
    protected $api_url;

    /**
     * @var array
     * [
     *      "data" => array,
     *      ""
     * ]
     */
    protected $response_preformatted_data = [];

    /**
     * @var bool
     */
    protected $lgpi_ssl_disabled = false;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var LoopiaPlatformCookieCrumbler
     */
    public $cookie;

    /**
     * @var Client
     */
    protected $client = null;

    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    protected $guzzle_response_object = null;


    /**
     * @return array
     */
    public function getResponsePreformattedData(): array
    {
        return $this->response_preformatted_data;
    }

    /**
     * @param array $response_preformatted_data
     */
    public function setResponsePreformattedData(array $response_preformatted_data): void
    {
        $this->response_preformatted_data = $response_preformatted_data;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders($headers = []): void
    {
        if (empty($this->headers)) {
            $this->init_request_headers();
        }

        $this->headers = array_merge($this->headers, $headers);
    }

    /**
     * @return bool
     */
    public function isLgpiSslDisabled(): bool
    {
        return $this->lgpi_ssl_disabled;
    }

    /**
     * @param bool $lgpi_ssl_disabled
     */
    public function setLgpiSslDisabled(bool $lgpi_ssl_disabled): void
    {
        $this->lgpi_ssl_disabled = $lgpi_ssl_disabled;
    }

    /**
     * @return \GuzzleHttp\Psr7\Response
     */
    public function getGuzzleResponseObject(): \GuzzleHttp\Psr7\Response
    {
        return $this->guzzle_response_object;
    }

    /**
     * @param \GuzzleHttp\Psr7\Response $guzzle_response_object
     */
    public function setGuzzleResponseObject(\GuzzleHttp\Psr7\Response $guzzle_response_object): void
    {
        $this->guzzle_response_object = $guzzle_response_object;
    }

    protected function init_request_headers()
    {
        $this->headers['Content-type'] = 'application/json; charset=UTF-8';
        $this->headers['Accept'] = 'application/json';
        $this->headers['X-Timestamp'] = date("c");

        if ($this->cookie->getName() && $this->cookie->getValue()) {
            // Set existing cookie in headers
            $cookie_str = $this->cookie->getName() . "=" . $this->cookie->getValue() . ";";
            $this->headers['Cookie'] = $cookie_str;
        }

        // set _ga header to be sent to API
        if ( isset( $_COOKIE['_ga'] ) && !empty($this->headers['Cookie']) ) {
            $this->headers['Cookie'] .= " _ga=".$_COOKIE['_ga'].";";
        }
    }

    /**
     * LoopiaPlatformCartApiClient constructor.
     * @param $api_url
     * @param bool $lgpi_ssl_disabled
     */
    public function __construct($api_url, $lgpi_ssl_disabled = false)
    {
        $this->api_url = $api_url;
        $this->lgpi_ssl_disabled = $lgpi_ssl_disabled;
        $this->cookie = new LoopiaPlatformCookieCrumbler();
        $this->client = new Client([
            'base_uri' => strval($this->api_url),
            'verify' => !$this->lgpi_ssl_disabled
        ]);
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $data
     * @param int $timeout
     * @return array|bool|false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call($method,$endpoint, $data = [], $timeout = 15.0)
    {
        try{
            $this->init_request_headers();
        	// DEBUG for headers
			// var_dump($this->headers);
            if (empty($this->headers)) {
                throw new Exception(LgpiExceptionMessages::HEADERS_NOT_SET);
            }
            $this->do_request($method, $endpoint, $data, $timeout);
            $this->extract_cookie();
            return $this->build_response_from_body();
        }catch (Exception $e) {
        	// DEBUG for failed response
        	// var_dump($e->getResponse()->getBody()->getContents());
            if( $e instanceof GuzzleHttp\Exception\ConnectException ){
        		// connection failed
        		$error = ['exeption' => 'ConnectException'];
	            $this->response_preformatted_data['error'] = json_encode($error);
	           	trigger_error(LgpiExceptionMessages::API_CONNECTION_ERROR.': ConnectException' . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
	            return $this->response_preformatted_data;
            }else if( is_object($e) ){
            	// get headers of response
            	$headers = $e->getResponse()->getHeaders();
            	// get content type of response
            	$content_type = $headers["Content-Type"];
            	if ( is_array( $headers["Content-Type"] ) ) {
            		$content_type = $content_type[0];
            	}
	        	if ( $content_type == 'application/json' ) {
	        		// we have json
		            $this->response_preformatted_data['error'] = $e->getResponse()->getBody()->getContents();
	           		trigger_error(LgpiExceptionMessages::API_CONNECTION_ERROR.': ' . $e->getResponse()->getBody()->getContents() . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
		            return $this->response_preformatted_data;
	        	}else{
	        		// response is not json
	        		$error = array();
	        		$error['code'] = $e->getResponse()->getStatusCode();
	        		$error['reason'] = $e->getResponse()->getReasonPhrase();
	        		$error['message'] = $e->getMessage();
	           		trigger_error(LgpiExceptionMessages::API_CONNECTION_ERROR.': ' . $e->getResponse()->getStatusCode() . ' - ' . $error['reason'] . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
	        		// var_dump($error);
	        		// $e->getResponse()->getReasonPhrase();
		            $this->response_preformatted_data['error'] = json_encode($error);
		            return $this->response_preformatted_data;
	        	}
	        	// var_dump($e->getMessage());
	        	// var_dump($e->getResponse());
	        	// var_dump($e->getResponse()->getBody()->getContents());

	        	// var_dump($e->getMessage() . " - " . $e->getFile() . "-" . $e->getLine());
	            // log exception
	            // trigger_error($e->getMessage() . " - " . $e->getFile() . "-" . $e->getLine() . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
	            // trigger_error($e->getResponse()->getBody()->getContents() . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
	            // $this->response_preformatted_data['error'] = $e->getMessage() . " - " . $e->getFile() . "-" . $e->getLine();
            }
            return false;
        }
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $data
     * @param $timeout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function do_request($method, $endpoint, array $data, $timeout): void
    {
        $request = new \GuzzleHttp\Psr7\Request(
            $method,
            $endpoint,
            $this->headers,
            json_encode($data)
        );

        // DEBUG for guzzle object / body
        // $body = $request->getBody();
        // $body->rewind();
        // var_dump( $body->getContents() );

        $this->guzzle_response_object = $this->client->send(
            $request, ['timeout' => $timeout]
        );
    }

    /**
     * @return array
     */
    protected function build_response_from_body()
    {
        $response_body = $this->guzzle_response_object->getBody()->getContents();
        // DEBUG for successfull response
        // var_dump($response_body);

        if ($this->guzzle_response_object->hasHeader('Content-type') && $this->guzzle_response_object->getHeader('Content-type')[0] == "application/json") {
            $this->response_preformatted_data['json'] = $response_body;
        } else {
            $this->response_preformatted_data['error'] = LgpiExceptionMessages::UNEXPECTED_RESPONSE;
            // trigger_error('unable to find correct headers in response' . " " . date('j.n.Y H:i:s'), E_USER_WARNING);
            // trigger_error('unable to find correct headers in response', E_USER_WARNING);
            // trigger_error('unable to find correct headers in response');
        }
        $response = $this->response_preformatted_data;

        return $response;
    }

    /**
     * extracts cookie from response headers
     */
    protected function extract_cookie()
    {
        if ($this->guzzle_response_object->hasHeader('Set-Cookie')) {
            $this->cookie->init_with_string($this->guzzle_response_object->getHeader('Set-Cookie')[0]);
        }
    }

    /**
     * @param string $response_code
     */
    protected function parse_response_code($response_code)
    {
        if ($response_code != '200' && $response_code != '201') {
            // $this->response_preformatted_data['error'] = "Responsecode: " . $response_code;
            // TODO: check response code, and create handlers for each if necessary
        }

        // handle 301 and 302 redirects
        if ($response_code == '301' || $response_code == '302') {
            # code...
        }
    }


    /**
     * API/GET | get the whole cart
     *
     * @return string/JSON
     */
    public function get_current_cart()
    {
        return $this->call('GET', 'cart');
    }


    /**
     * API/POST | put item to cart
     * @param $code *
     * @param $properties
     * @param $sellType
     * @return array|false|string|null
     */
    public function add_item_to_cart($code, $properties = [], $sellType = "")
    {
        $data = [
            "code" => $code,
            "properties" => $properties
        ];
        if (!empty($sellType)) {
        	$data["sellType"] = $sellType;
        }
        return $this->call('POST', 'cart/items', $data);
    }

    /**
     * API/DELETE | remove item from cart
     * @param string $itemid
     * @return array|false|string
     */
    public function remove_item_from_cart($itemid = '')
    {
        if (empty($itemid)) {
            $error['error'] = LgpiExceptionMessages::MISSING_ITEM_ID;
            return $error;
        }

        return $this->call('DELETE','cart/items/' . $itemid);
    }

    public function logout()
    {
        return $this->call('GET','logout');
    }

    public function get_user_details()
    {
        return $this->call('GET','users');
    }

    public function checkout()
    {
        return $this->call('POST','checkout');
    }
}
