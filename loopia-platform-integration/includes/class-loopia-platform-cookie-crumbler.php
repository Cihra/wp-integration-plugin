<?php

defined( 'ABSPATH' ) || exit;

class LoopiaPlatformCookieCrumbler
{
    protected $string;
    protected $name;
    protected $value;
    protected $data = [];

    const COOKIE_OPTIONS = ['expires', 'path', 'domain', 'secure', 'httponly'];

    /**
     * @return mixed
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * @param mixed $string
     */
    public function setString($string): void
    {
        $this->string = $string;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function __construct()
    {
    	if ( isset( $_COOKIE['cart_session'] ) ) {
    		// Check if we have a cookie and set it
    	    $this->setName('cart_session');
    	    $this->setValue($_COOKIE['cart_session']);
    	}
    }

    public function init_with_string($string)
    {
        if(!$string){
            return false;
        }
        $this->string = $string;
        $this->extract_cookie_info($string);
        $this->set_cookie_on_server();
    }

    /**
     * Sets cookie on server
     * @return bool
     */
    protected function set_cookie_on_server()
    {
        if(!empty($this->name) && !empty($this->value) && !empty($this->data)){
            setcookie(
                $this->name,
                $this->value,
                $this->data
            );
            return true;
        }

        return false;
    }

    /**
     * Extracts cookie info
     * @param $cookie_string
     */
    protected function extract_cookie_info($cookie_string)
    {
        $cookie_properties = explode('; ',$cookie_string);

        foreach ($cookie_properties as $property) {
            $property_name = $property;
            $property_value = null;
            if(strstr($property, '=')){
                list($property_name, $property_value) = explode('=',$property);
            }

            switch ($property_name){
                case 'cart_session':
                    $this->name = $property_name;
                    $this->value = $property_value;
                    break;
                case $property_value == null:
                    $this->data[$property_name] = true;
                    break;
                case in_array($property_name,self::COOKIE_OPTIONS) :
                    $this->data[$property_name] = $property_value;
                    break;
            }
        }

        if(!key_exists('secure',$this->data)){
            $this->data['secure'] = false;
        }

        if(!key_exists('httponly',$this->data)){
            $this->data['httponly'] = false;
        }
    }
}
