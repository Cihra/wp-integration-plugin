<?php
/**
 * LoopiaPlatform setup
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Main LoopiaPlatform Class
 *
 * @class LoopiaPlatform
 */
class LoopiaPlatform {

	/**
	 * LoopiaPlatform version.
	 *
	 * @var string
	 */
	public $version = '0.8.8';

    /**
     * @var LgpiSettingsCache
     */
	protected $lgpi_settings_cache;

    /**
     * @var LoopiaPlatformCartApiClient
     */
	private $cart_api_client = null;

    /**
     * @return LoopiaPlatformCartApiClient
     */
    public function getCartApiClient(): LoopiaPlatformCartApiClient
    {
        return $this->cart_api_client;
    }

    /**
     * @param LoopiaPlatformCartApiClient $cart_api_client
     */
    public function setCartApiClient(LoopiaPlatformCartApiClient $cart_api_client): void
    {
        $this->cart_api_client = $cart_api_client;
    }



    /**
     * @return LgpiSettingsCache
     */
    public function getLgpiSettingsCache(): LgpiSettingsCache
    {
        return $this->lgpi_settings_cache;
    }

    /**
     * @param LgpiSettingsCache $lgpi_settings_cache
     */
    public function setLgpiSettingsCache(LgpiSettingsCache $lgpi_settings_cache): void
    {
        $this->lgpi_settings_cache = $lgpi_settings_cache;
    }

	/**
	 * allowed shortcode attributes/properties for ADD_TO_CART
	 * specify allowed properties as per https://git.websupport.sk/devel/cart-api/-/blob/master/docs/SupportedProducts.md
	 *
	 * @var array
	 */
	protected static $allowed_shortcode_atts = array(
		'domain' => 		array('type' => 'string', 'original_name' => 'domain'),
		'capacity' => 		array('type' => 'int', 'original_name' => 'capacity'),
		'cpu' => 			array('type' => 'int', 'original_name' => 'cpu'),
		'epp' => 			array('type' => 'string', 'original_name' => 'epp'),
		'domainsCount' => 	array('type' => 'int', 'original_name' => 'domainsCount'),
		'domainscount' => 	array('type' => 'int', 'original_name' => 'domainsCount'),
		'ip' => 			array('type' => 'string', 'original_name' => 'ip'),
		'localPart' => 		array('type' => 'string', 'original_name' => 'localPart'),
		'localpart' => 		array('type' => 'string', 'original_name' => 'localPart'),
		'name' => 			array('type' => 'string', 'original_name' => 'name'),
		'osName' => 		array('type' => 'string', 'original_name' => 'osName'),
		'osname' => 		array('type' => 'string', 'original_name' => 'osName'),
		'osTemplate' => 	array('type' => 'string', 'original_name' => 'osTemplate'),
		'ostemplate' => 	array('type' => 'string', 'original_name' => 'osTemplate'),
		'snapshotSlot' => 	array('type' => 'int', 'original_name' => 'snapshotSlot'),
		'snapshotslot' => 	array('type' => 'int', 'original_name' => 'snapshotSlot'),
		'ram' => 			array('type' => 'int', 'original_name' => 'ram'),
		'type' => 			array('type' => 'string', 'original_name' => 'type'),
		'management' => 	array('type' => 'bool', 'original_name' => 'management'),
		'backup' => 		array('type' => 'bool', 'original_name' => 'backup'),
		'monitoring' => 	array('type' => 'bool', 'original_name' => 'monitoring'),
		'packageId' => 		array('type' => 'string', 'original_name' => 'packageId'),
		'packageid' => 		array('type' => 'string', 'original_name' => 'packageId')
	);

	/**
	 * Allowed ajax actions
	 *
	 * @var array
	 */
	protected static $lgpi_ajax_events_nopriv = array(
		'lgpi_add_to_cart',
		'lgpi_orderflow',
		'lgpi_remove_from_cart',
		'lgpi_update_cart',
		'lgpi_get_cart_item_count',
		'lgpi_user_details',
		'lgpi_user_logout',
	);


	/**
	 * The single instance of the class.
	 *
	 * @var LoopiaPlatform
	 */
	protected static $_instance = null;


    public function __clone() {
        // Cloning is forbidden.
    }

    public function __wakeup() {
        // Unserializing instances of this class is forbidden.
    }

    /**
     * Include required frontend files.
     */
    public function frontend_includes() {
        // Initialize shortcodes
        include_once LGPI_ABSPATH . 'includes/shortcodes.php';
    }

    /**
     * Include admin files.
     */
    public function admin_includes() {
        // Create admin menus
        include_once LGPI_ABSPATH . 'includes/class-admin-menus.php';
    }

    /**
     * Load Localisation files.
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'LGPI', false, plugin_basename( dirname( LGPI_PLUGIN_FILE ) ) . '/languages' );
    }

    /**
     * Get the plugin url.
     *
     * @return string
     */
    public function plugin_url() {
        return untrailingslashit( plugins_url( '/', LGPI_PLUGIN_FILE ) );
    }


    /**
     * Get the plugin path.
     *
     * @return string
     */
    public function plugin_path() {
        return untrailingslashit( plugin_dir_path( LGPI_PLUGIN_FILE ) );
    }


    /**
     * Get Ajax URL.
     *
     * @return string
     */
    public function ajax_url() {
        return admin_url( 'admin-ajax.php', 'relative' );
    }

	/**
	 * Main Instance.
	 *
	 * Ensures only one instance of LoopiaPlatform is loaded or can be loaded.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->define_constants();
        $this->includes();

        $this->lgpi_settings_cache = new LgpiSettingsCache();
        $this->init_settings();
		$this->register_settings();
        $this->init_cart_api_client();

        $this->init_hooks();
	}

    /**
     * Define constant if not already set.
     *
     * @param string      $name  Constant name.
     * @param string|bool $value Constant value.
     */
    private function define( $name, $value ): void
    {
        if ( ! defined( $name ) ) {
            define( $name, $value );
        }
    }

    /**
     * Define Constants.
     */
    protected function define_constants(): void
    {
        // $upload_dir = wp_upload_dir( null, false );
        $this->define( 'LGPI_ABSPATH', dirname( LGPI_PLUGIN_FILE ) . '/' );
        $this->define( 'LGPI_PLUGIN_BASENAME', plugin_basename( LGPI_PLUGIN_FILE ) );
        $this->define( 'LGPI_VERSION', $this->version );
        // $this->define( 'LGPI_SESSION_CACHE_GROUP', 'LGPI_session_id' );
    }

    /**
     * Initialize settings for plugin.
     */
    protected function init_settings(): void
    {
        $lgpi_cart_api_url_text = '';
        $lgpi_ssl_disabled_text = '';
        $lgpi_checkout_api_url_text = '';
        $lgpi_checkout_asset_url_text = '';
        $lgpi_checkout_login_url_text = '';
        $lgpi_checkout_logout_url_text = '';
        $lgpi_checkout_env_text = '';
        $settings['lgpi_login_area_heading'] = new LgpiSetting('Settings for Login area dropdown','heading', false, '' );
        $settings['lgpi_login_my_account_link'] = new LgpiSetting('Link to account settings in user dropdown','text', true, get_option('lgpi_login_my_account_link'), 'Full url to account settings' );
        $settings['lgpi_login_my_services_link'] = new LgpiSetting('Link to my services in user dropdown','text', true, get_option('lgpi_login_my_services_link'), 'Full url to my services' );
        $settings['lgpi_login_invoices_link'] = new LgpiSetting('Link to invoice overview in user dropdown','text', true, get_option('lgpi_login_invoices_link'), 'Full url to invoice overview' );

        $settings['lgpi_currency_heading'] = new LgpiSetting('Settings for default currencies','heading', false, '' );
        $settings['lgpi_vat_amount'] = new LgpiSetting('VAT amount for product prices','text', true, get_option('lgpi_vat_amount'), 'For example <code>1.25</code> equals 25% VAT. It is crucial to set this correctly! ' );
        $settings['lgpi_currency_symbol'] = new LgpiSetting('Symbol to use with product prices','text', true, get_option('lgpi_currency_symbol'), 'For example <code>kr</code> this will be displayed when using the price shortcode. ' );

        $settings['lgpi_cart_api_heading'] = new LgpiSetting('Settings for CART API','heading', false, '' );
        if ($this->is_env_setting('lgpi_cart_api_url')) { $lgpi_cart_api_url_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_cart_api_url').'</code>. '; }
        $settings['lgpi_cart_api_url'] = new LgpiSetting('Cart API url','text', true, $this->get_env_setting('lgpi_cart_api_url'), $lgpi_cart_api_url_text );
        $settings['lgpi_cart_api_exponea'] = new LgpiSetting('Cart API Exponea token','text', true, get_option('lgpi_cart_api_exponea') );
        if ($this->is_env_setting('lgpi_ssl_disabled')) { $lgpi_ssl_disabled_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_ssl_disabled').'</code>. '; }
        $settings['lgpi_ssl_disabled'] = new LgpiSetting('Disable SSL peer verification','check', true, $this->get_env_setting('lgpi_ssl_disabled'), $lgpi_ssl_disabled_text );
        $settings['lgpi_google_places_api_key'] = new LgpiSetting('Google Places API key','text', true, get_option('lgpi_google_places_api_key') );

        $settings['lgpi_checkout_api_heading'] = new LgpiSetting('Settings for Checkout/Orderflow embedding','heading', false, '' );
        if ($this->is_env_setting('lgpi_checkout_api_url')) { $lgpi_checkout_api_url_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_checkout_api_url').'</code>. '; }
        $settings['lgpi_checkout_api_url'] = new LgpiSetting('Orderflow API url','text', true, $this->get_env_setting( 'lgpi_checkout_api_url' ), $lgpi_checkout_api_url_text );
        $settings['lgpi_checkout_base_url'] = new LgpiSetting('Orderflow base url','text', true, get_option('lgpi_checkout_base_url') );

        if ($this->is_env_setting('lgpi_checkout_asset_url')) { $lgpi_checkout_asset_url_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_checkout_asset_url').'</code>. '; }
        $settings['lgpi_checkout_asset_url'] = new LgpiSetting('Orderflow asset url','text', true, $this->get_env_setting('lgpi_checkout_asset_url'), $lgpi_checkout_asset_url_text );
        if ($this->is_env_setting('lgpi_checkout_login_url')) { $lgpi_checkout_login_url_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_checkout_login_url').'</code>. '; }
        $settings['lgpi_checkout_login_url'] = new LgpiSetting('Orderflow login url','text', true, $this->get_env_setting('lgpi_checkout_login_url'), $lgpi_checkout_login_url_text );
        if ($this->is_env_setting('lgpi_checkout_logout_url')) { $lgpi_checkout_logout_url_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_checkout_logout_url').'</code>. '; }
        $settings['lgpi_checkout_logout_url'] = new LgpiSetting('Orderflow logout url','text', true, $this->get_env_setting('lgpi_checkout_logout_url'), $lgpi_checkout_logout_url_text );
        $settings['lgpi_checkout_knowledgebase_url'] = new LgpiSetting('Orderflow support url','text', true, get_option('lgpi_checkout_knowledgebase_url') );
        // It was found out that these two were merged to supportUrl and only one is needed
        // $settings['lgpi_checkout_helpdesk_url'] = new LgpiSetting('Orderflow helpDesk url','text', true, get_option('lgpi_checkout_helpdesk_url') );
        $settings['lgpi_checkout_webadmin_url'] = new LgpiSetting('Orderflow webAdmin url','text', true, get_option('lgpi_checkout_webadmin_url') );
        $settings['lgpi_checkout_hostings_url'] = new LgpiSetting('Orderflow Hosting packages url','text', true, get_option('lgpi_checkout_hostings_url') );
        $settings['lgpi_checkout_webadmin_base_invoice_url'] = new LgpiSetting('Orderflow webAdmin/invoice url','text', true, get_option('lgpi_checkout_webadmin_base_invoice_url'), 'For example <code>https://admin.websupport.se/invoice/detail/</code> ' );
        $settings['lgpi_checkout_tos_url'] = new LgpiSetting('Orderflow TOS url','text', true, get_option('lgpi_checkout_tos_url'), 'This can be translated for multiple languages in WPML string translation.' );
        $settings['lgpi_checkout_migrationinfo_url'] = new LgpiSetting('Orderflow migrationInfo url','text', true, get_option('lgpi_checkout_migrationinfo_url'), 'This can be translated for multiple languages in WPML string translation.' );
        $settings['lgpi_checkout_transfer_url'] = new LgpiSetting('Orderflow transfer url','text', true, get_option('lgpi_checkout_transfer_url'), 'This can be translated for multiple languages in WPML string translation.' );
        $settings['lgpi_checkout_company'] = new LgpiSetting('Orderflow company','text', true, get_option('lgpi_checkout_company') );
        $settings['lgpi_checkout_languages'] = new LgpiSetting('Orderflow supported languages','text', true, get_option('lgpi_checkout_languages'), 'For example <code>{ "sv-SE": "Swedish", "en-US": "English" }</code> ' );
        // $settings['lgpi_checkout_tos_urls'] = new LgpiSetting('Orderflow map of TOS urls per supported language','text', true, get_option('lgpi_checkout_tos_urls'), 'For example <code>{ "sv": "example.se/tos", "en": "example.se/en/tos" }</code> ' );
        $settings['lgpi_checkout_currency'] = new LgpiSetting('Orderflow currency','text', true, get_option('lgpi_checkout_currency') );
        $settings['lgpi_checkout_company_country'] = new LgpiSetting('Orderflow company Country','text', true, get_option('lgpi_checkout_company_country') );
        $settings['lgpi_checkout_workflow'] = new LgpiSetting('Orderflow workflow','text', true, get_option('lgpi_checkout_workflow') );
        if ($this->is_env_setting('lgpi_checkout_env')) { $lgpi_checkout_env_text = 'Env var override: <code>'. $this->get_env_setting('lgpi_checkout_env').'</code>. '; }
        $settings['lgpi_checkout_env'] = new LgpiSetting('Orderflow environment','text', true, $this->get_env_setting('lgpi_checkout_env'), $lgpi_checkout_env_text.'Values for example <code>staging</code> or (defaults to) <code>production</code>' );
        $settings['lgpi_checkout_theme'] = new LgpiSetting('Orderflow theme','text', true, get_option('lgpi_checkout_theme'), 'Use <code>cary</code> for new design. Defaults to legacy theme' );
        $settings['lgpi_checkout_gtag'] = new LgpiSetting('Orderflow gtag','text', true, get_option('lgpi_checkout_gtag') );
        $settings['lgpi_internal_checkout_page_url'] = new LgpiSetting('Internal checkout page url (within wordpress)','text', true, get_option('lgpi_internal_checkout_page_url'), 'Specify the relative page url. For example <code>cart/</code> without the base url of the website.' );


            	/*
        			"invoiceUrl": "https://admin.websupport.sk/invoce/EWFwddfEFfqfqa", // required after failed payment, so user can pay again via admin
            	*/

        $this->lgpi_settings_cache->setSettingsData($settings);

    }

    /**
     * Getter for environment specific API definitions
     * Will override WP DB options
     *
     * @param string     	$setting_name  option name
     * @return string|bool 	false if option does not exist
     */
    protected function get_env_setting( string $setting_name){
		switch ($setting_name) {
			case 'lgpi_cart_api_url':
				if (defined('WB_SET_LGPI_CART_API_URL')) { return WB_SET_LGPI_CART_API_URL; }
				break;
			case 'lgpi_checkout_api_url':
				if (defined('WB_SET_LGPI_CHECKOUT_API_URL')) { return WB_SET_LGPI_CHECKOUT_API_URL; }
				break;
			case 'lgpi_checkout_asset_url':
				if (defined('WB_SET_LGPI_CHECKOUT_ASSET_URL')) { return WB_SET_LGPI_CHECKOUT_ASSET_URL; }
				break;
			case 'lgpi_checkout_login_url':
				if (defined('WB_SET_LGPI_CHECKOUT_LOGIN_URL')) { return WB_SET_LGPI_CHECKOUT_LOGIN_URL; }
				break;
			case 'lgpi_checkout_logout_url':
				if (defined('WB_SET_LGPI_CHECKOUT_LOGOUT_URL')) { return WB_SET_LGPI_CHECKOUT_LOGOUT_URL; }
				break;
			case 'lgpi_checkout_env':
				if (defined('WB_SET_LGPI_CHECKOUT_ENV')) { return WB_SET_LGPI_CHECKOUT_ENV; }
				break;
			case 'lgpi_ssl_disabled':
				if (defined('WB_SET_LGPI_SSL_DISABLED')) { return WB_SET_LGPI_SSL_DISABLED; }
				break;
			default:
				return get_option($setting_name);
				break;
		}
		return get_option($setting_name);
	}

    /**
     * Check if setting is using ENV value
     *
     * @param string     	$setting_name  option name
     * @return bool 	true|false
     */
    protected function is_env_setting( string $setting_name){
		switch ($setting_name) {
			case 'lgpi_cart_api_url':
				if (defined('WB_SET_LGPI_CART_API_URL')) { return true; }
				break;
			case 'lgpi_checkout_api_url':
				if (defined('WB_SET_LGPI_CHECKOUT_API_URL')) { return true; }
				break;
			case 'lgpi_checkout_asset_url':
				if (defined('WB_SET_LGPI_CHECKOUT_ASSET_URL')) { return true; }
				break;
			case 'lgpi_checkout_login_url':
				if (defined('WB_SET_LGPI_CHECKOUT_LOGIN_URL')) { return true; }
				break;
			case 'lgpi_checkout_logout_url':
				if (defined('WB_SET_LGPI_CHECKOUT_LOGOUT_URL')) { return true; }
				break;
			case 'lgpi_checkout_env':
				if (defined('WB_SET_LGPI_CHECKOUT_ENV')) { return true; }
				break;
			case 'lgpi_ssl_disabled':
				if (defined('WB_SET_LGPI_SSL_DISABLED')) { return true; }
				break;
			default:
				return false;
				break;
		}
		return false;
	}

    /**
     * Include required core files used in admin and on the frontend.
     */
    protected function includes(): void
    {

        include_once LGPI_ABSPATH . 'includes/class-loopia-platform-cart-api-client.php';
        include_once LGPI_ABSPATH . 'includes/class-lgpi-setting.php';
        include_once LGPI_ABSPATH . 'includes/class-lgpi-settings-cache.php';
        include_once LGPI_ABSPATH . 'includes/class-lgpi-post-type-setup.php';

        if ( $this->is_request( 'admin' ) ) {
            $this->admin_includes();
        }

        if ( $this->is_request( 'frontend' ) ) {
            $this->frontend_includes();
        }
    }

    public function init_cart_api_client(): void
    {
        $this->cart_api_client = new LoopiaPlatformCartApiClient(
            $this->lgpi_settings_cache->getSettingsData()['lgpi_cart_api_url']->getValue(),
            $this->lgpi_settings_cache->getSettingsData()['lgpi_ssl_disabled']->getValue()
        );
    }

	/**
	 * Hook into actions and filters.
	 */
    protected function init_hooks(): void
    {
		// These are currently unused
		// register_activation_hook( LGPI_PLUGIN_FILE, array( $this, 'on_activation' ) );
		// add_action( 'plugins_loaded', array( $this, 'on_plugins_loaded' ), -1 );
		// add_action( 'after_setup_theme', array( $this, 'after_theme_setup' ), 11 );

		add_action( 'init', array( $this, 'init' ), 10 );
		// AJAX
		add_action( 'init', array( $this, 'lgpi_define_ajax' ), 0 );
		add_action( 'template_redirect', array( $this, 'do_lgpi_ajax' ), 0 );
		$this->lgpi_add_ajax_events();
		add_action( 'wp_print_footer_scripts', array( $this, 'lgpi_localize_scripts' ), 5 );
	}

	/**
	 * register settings
	 */
	public function register_settings(): void
	{
		foreach ($this->lgpi_settings_cache->getSettingsData() as $option => $setting_object) {
            /**
             * @var LgpiSettings $option
             */
			if ($setting_object->getSaveAsOptions()) {
				register_setting( 'lgpi_settings', $option, ['string', '', null, false, ''] );
			}
		}
	}



	/**
	 * get settings
	 *
	 */
	public function get_settings() {
		return $this->lgpi_settings_cache->getSettingsData();
	}


    protected function register_google_places_sdk(): void
    {
    	$api_key = $this->lgpi_settings_cache->getSettingsData()[ 'lgpi_google_places_api_key' ]->getValue();
    	if( $api_key ){
	    	$google_places_sdk_url = 'https://maps.googleapis.com/maps/api/js?key='.$api_key.'&amp;libraries=places';
	        wp_register_script('lgpi_gplaces_sdk', $google_places_sdk_url, ['jquery'], $this->version, true);
    	}
    }

    protected function register_cart_script(): void
    {
        // scripts
        wp_register_script('lgpi_cart_js', $this->plugin_url() . '/assets/js/cart.min.js', ['jquery'], $this->version, true);
        // styles
        wp_register_style('lgpi_frontend', $this->plugin_url() . "/assets/css/lgpi-styles.css", false, $this->version);
    }

    protected function enqueue_cart_scripts(): void
    {
        // enqueue scripts,styles and add cart HTML
        wp_enqueue_style("lgpi_frontend");
        wp_enqueue_script("lgpi_cart_js");
    }

    protected function enqueue_spa_scripts(): void
    {
    	wp_enqueue_script("lgpi_checkout_app_js");
    	wp_enqueue_script("lgpi_checkout_chunk_vendors_js");
    	wp_enqueue_script("lgpi_gplaces_sdk");
    	wp_enqueue_script("lgpi_checkout_theme_js");
    }

    protected function enqueue_spa_styles(): void
    {
    	wp_enqueue_style("lgpi_checkout_app");
    	wp_enqueue_style("lgpi_checkout_chunk_vendors");
    	wp_enqueue_style("lgpi_checkout_theme");
    }

    /**
     * Init plugin when WordPress Initialises.
     */
    public function init(): void
    {

    	$this->register_cart_script();
        $this->register_google_places_sdk();

        // Set up localisation.
        $this->load_plugin_textdomain();

        // Register Actions loaded for the Frontend (not login). Also make sure we are NOT doing ajax.
        if ( $this->is_request( 'frontend' )) {
            $this->enqueue_cart_scripts();
            add_action( 'wp_footer', array( $this, 'lgpi_print_frontend_cart' ), 100 );
        }
    }

	/**
	 * get registered LGPI options for specific functionality
	 *
	 * @param string      $option_set  eg. "checkout" to get an array of checkout params
	 * @return array|bool
	 */
    public function get_lgpi_options($option_set = "") {
		if ( !empty($option_set) ){
			switch ($option_set) {
				case 'checkout':
                    $settings = $this->lgpi_settings_cache->get_checkout_settings();

                    // TODO/IMPROVE "lgpi_checkout_base_url" -> this must be set individually per language (WPML)
                    // NOTE this makes "lgpi_checkout_base_url"-option obsolete as it uses the internal checkout page url
                    if( WP_HOME && function_exists('icl_get_languages') ){
                        // override the static baseurl with dynamic baseurl
                        $lgpi_checkout_base_url = str_replace(WP_HOME, '', $this->get_checkout_url());

                        if(key_exists('baseUrl', $settings)){
                            $settings['baseUrl'] = $lgpi_checkout_base_url;
                        }
                    }
                    return empty($settings) ? false : $settings;
					break;
			}
		}
		return false;
	}


	/**
	 * get allowed_shortcode_atts
	 *
	 * @return array
	 */
	public static function get_allowed_shortcode_atts(): array
	{
		return self::$allowed_shortcode_atts;
	}


	/**
	 * get cart/checkout URL
	 *
	 * @return string
	 */
	public function get_checkout_url(): string
	{
		$lgpi_internal_checkout_page_url = $this->lgpi_settings_cache->get_checkout_url();
		if ( !empty($lgpi_internal_checkout_page_url) ) {
			return home_url($lgpi_internal_checkout_page_url);
		}
		// internal page url not defined..
		return home_url();
	}

	/**
	 * What type of request is this?
	 *
	 * @param  string $type admin, ajax, cron or frontend.
	 * @return bool
	 */
	public function is_request( $type ): bool
	{
		switch ( $type ) {
			case 'admin':
				return is_admin();
			case 'ajax':
				return defined( 'DOING_AJAX' );
			case 'cron':
				return defined( 'DOING_CRON' );
			case 'frontend':
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) &&
                    ! defined( 'DOING_CRON' ) &&
                    isset($GLOBALS['pagenow']) &&
                    $GLOBALS['pagenow'] !== 'wp-login.php';
		}
	}

	/**
	 * Get contents of SPA manifest
	 *
	 * @return string/bool
	 */
    public function get_manifest()
    {
        $lgpi_checkout_asset_url =	$this->lgpi_settings_cache->getSettingsData()[ 'lgpi_checkout_asset_url' ]->getValue();
        $lgpi_ssl_disabled =		$this->lgpi_settings_cache->getSettingsData()[ 'lgpi_ssl_disabled' ]->getValue();
        if ($lgpi_checkout_asset_url) {
        	if ( defined('WB_SET_CACHE_SPA_ASSET_MANIFEST_LOCATION') && file_exists(WB_SET_CACHE_SPA_ASSET_MANIFEST_LOCATION) ) { 
        		// Check if we have separate cached manifest in the environment
	        	return json_decode( @file_get_contents(WB_SET_CACHE_SPA_ASSET_MANIFEST_LOCATION), true );
        	}

	        if ( $lgpi_ssl_disabled == '1' ) {
	        	$steam_context_options = array(
	        	    "ssl" => array(
	        	        "verify_peer"=>false,
	        	        "verify_peer_name"=>false,
	        	    ),
	        	);
	        	$manifest = @file_get_contents($lgpi_checkout_asset_url."/manifest.json", false, stream_context_create($steam_context_options));
	        }else{
	        	$manifest = @file_get_contents($lgpi_checkout_asset_url."/manifest.json");
	        }

	        return json_decode( $manifest, true );
        }
        return false;
    }

    /**
     * Validate manifest
     *
     * @return bool
     */
    public function is_manifest_data_valid($manifest): bool
    {
        if($manifest === FALSE) {
            // manifest error
            return false;
        }else {
            if (isset($manifest['chunk-vendors.js']) && isset($manifest['app.js']) && isset($manifest['app.css'])) {
                return true;
            }
        }
    }


    /**
     * Initialize required assets for checkout/orderflow shortcode embed
     *
     * The assets are declared in the asset manifest.json
     *
     * @return bool
     */
    public function init_checkout_assets(): bool
    {
        if($manifest = $this->get_manifest()){
            if($this->is_manifest_data_valid($manifest)){
            	// required
                $lgpi_checkout_asset_url =  $this->lgpi_settings_cache->getSettingsData()['lgpi_checkout_asset_url']->getValue();
                $lgpi_checkout_app_js_url = $lgpi_checkout_asset_url.$manifest['app.js'];
                $lgpi_checkout_chunk_vendors_js_url = $lgpi_checkout_asset_url.$manifest['chunk-vendors.js'];
                $lgpi_checkout_app_css_url = $lgpi_checkout_asset_url.$manifest['app.css'];
                wp_register_style( 'lgpi_checkout_app', $lgpi_checkout_app_css_url, false, $this->version );
                wp_register_script( 'lgpi_checkout_app_js', $lgpi_checkout_app_js_url, '', $this->version, true );
                wp_register_script( 'lgpi_checkout_chunk_vendors_js', $lgpi_checkout_chunk_vendors_js_url, '', $this->version, true );
                if (isset($manifest['chunk-vendors.css'])) {
					// optional
                    $lgpi_checkout_chunk_vendors_css_url = $lgpi_checkout_asset_url.$manifest['chunk-vendors.css'];
                    wp_register_style( 'lgpi_checkout_chunk_vendors', $lgpi_checkout_chunk_vendors_css_url, false, $this->version );
                }
                // check theme
                $lgpi_checkout_theme =  $this->lgpi_settings_cache->getSettingsData()['lgpi_checkout_theme']->getValue();
                if ( $lgpi_checkout_theme == 'cary' ) {
					if(isset($manifest['themeCary.css'])){ 
						$lgpi_checkout_theme_css_url = $lgpi_checkout_asset_url.$manifest['themeCary.css'];
						wp_register_style( 'lgpi_checkout_theme', $lgpi_checkout_theme_css_url, false, $this->version );
					 }
					//  not needed right now -> seems to be autogenerated by webpack
					// if(isset($manifest['themeCary.js'])){ 
					// 	$lgpi_checkout_theme_js_url = $lgpi_checkout_asset_url.$manifest['themeCary.js'];
					// 	wp_register_script( 'lgpi_checkout_theme_js', $lgpi_checkout_theme_js_url, false, $this->version );
					//  }
                }else{
					// legacy theme
					// if(isset($manifest['themeLegacy.css'])){ 
					// 	$lgpi_checkout_theme_css_url = $lgpi_checkout_asset_url.$manifest['themeLegacy.css'];
					// 	wp_register_style( 'lgpi_checkout_theme', $lgpi_checkout_theme_css_url, false, $this->version );
					// }
					// if(isset($manifest['themeLegacy.js'])){ 
					// 	$lgpi_checkout_theme_js_url = $lgpi_checkout_asset_url.$manifest['themeLegacy.js'];
					// 	wp_register_script( 'lgpi_checkout_theme_js', $lgpi_checkout_theme_js_url, false, $this->version );
					//  }
				}
                // Needs to return info for shortcode
                return true;
            }
        }
        return false;
    }

    /**
     * Send headers for LGPI Ajax Requests.
     *
     */
    private function lgpi_ajax_response_headers() {
        if ( ! headers_sent() ) {
            send_origin_headers();
            send_nosniff_header();
            nocache_headers();
            header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
            header( 'X-Robots-Tag: noindex' );
            status_header( 200 );
        } elseif ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
            headers_sent( $file, $line );
            trigger_error( "lgpi_ajax_response_headers cannot set headers - headers already sent by {$file} on line {$line}", E_USER_NOTICE );
        }
    }

    /**
     * Hook in methods - uses WordPress ajax handlers (admin-ajax).
     */
    public function lgpi_add_ajax_events() {
        foreach ( self::$lgpi_ajax_events_nopriv as $ajax_event ) {
            add_action( 'wp_ajax_lgpi_' . $ajax_event, array( $this, $ajax_event ) );
            add_action( 'wp_ajax_nopriv_lgpi_' . $ajax_event, array( $this, $ajax_event ) );

            // lgpi_ajax_ can be used for frontend ajax requests.
            add_action( 'lgpi_ajax_' . $ajax_event, array( $this, $ajax_event ) );
        }
    }

	/**
	 * Set LGPI AJAX constant and headers.
	 */
	public static function lgpi_define_ajax() {
		if ( ! empty( $_GET['lgpi-ajax'] ) ) {
			define( 'DOING_AJAX', true );
			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {
				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON.
			}
			$GLOBALS['wpdb']->hide_errors();
		}
	}


	/**
	 * Check for LGPI Ajax request and fire action.
	 */
	public function do_lgpi_ajax() {
		global $wp_query;

		if ( ! empty( $_GET['lgpi-ajax'] ) ) {
			$wp_query->set( 'lgpi-ajax', sanitize_text_field( wp_unslash( $_GET['lgpi-ajax'] ) ) );
		}

		$action = $wp_query->get( 'lgpi-ajax' );

		if ( $action ) {
			// add check for existing actions
			if ( in_array($action, self::$lgpi_ajax_events_nopriv) ) {
				$this->lgpi_ajax_response_headers();
				$action = sanitize_text_field( $action );
				do_action( 'lgpi_ajax_' . $action );
				wp_die();
			}
		}
	}


	/**
	 * lgpi_localize_scripts
	 *
	 */
	public function lgpi_localize_scripts(  ) {
		wp_localize_script( 'lgpi_cart_js', 'lgpi_params', array( 'ajax_url' => $this->ajax_url(), 'lgpi_ajax_url' => '?lgpi-ajax=%%endpoint%%' ) );
	}

	/**
	 * AJAX connect to the users endpoint in API to get userdata

     * $response = [
     *  "logged-in" => BOOL
     *  "user-data" => [
     *      "username" => ""
     * ]
     * ]
	 */
	public function lgpi_user_details() {
        ob_start();
        $result = [
            "logged-in" => "false",
            "user-data" => []
        ];

		$response = $this->cart_api_client->get_user_details();
        $data = $this->parse_cart_api_response($response);
        if ( $data && $data['results'] == "success" ) {
        	if ( isset($data['login']) ) {
        	    $result['logged-in'] = "true";
        	    $result['user-data']['username'] = $data['login'];
	        	if ( isset($data['emails']) ) {
	        		if ( is_array($data['emails']) ) {
	        			// pick first email
	        	   		$result['user-data']['email'] = $data['emails'][0];
	        		}
	        	}
	        	// get urls for user dropdown
	        	$result['user-data']['url-services'] = __($this->lgpi_settings_cache->getSettingsData()[ 'lgpi_login_my_services_link' ]->getValue(), 'LGPI');
	        	$result['user-data']['name-services'] = __('My services','LGPI');
	        	$result['user-data']['url-invoices'] = __($this->lgpi_settings_cache->getSettingsData()[ 'lgpi_login_invoices_link' ]->getValue(), 'LGPI');
	        	$result['user-data']['name-invoices'] = __('Invoice overview','LGPI');
	        	$result['user-data']['url-account'] = __($this->lgpi_settings_cache->getSettingsData()[ 'lgpi_login_my_account_link' ]->getValue(), 'LGPI');
	        	$result['user-data']['name-account'] = __('Account settings','LGPI');
	        	$result['user-data']['url-admin'] = __($this->lgpi_settings_cache->getSettingsData()[ 'lgpi_checkout_webadmin_url' ]->getValue(), 'LGPI');
	        	$result['user-data']['name-admin'] = __('WebAdmin','LGPI');
        	}
        }
        if ( $data && $data['results'] == "error" ) {
            $result['error'] = $data;
        }

        echo json_encode($result);
	}


	/**
	 * AJAX logout a user
	 */
	public function lgpi_user_logout() {
		ob_start();

		$response = $this->cart_api_client->logout();
        // $this->parse_cart_api_response($response);
    }


	/**
	 * AJAX add to cart.
	 */
	public function lgpi_add_to_cart() {
		ob_start();
		$result = [
		    "added-to-cart" => "false",
		];

		$selltype = '';
		$product_code = '';

		// itemid required
		if ( ! isset( $_POST['product_code'] ) ) {
			// Product code is missing.
			$result['warning'] = "product-is-missing";
			echo json_encode($result);
		}else{
			// TODO: do we need to sanitize this?
			$product_code = (String)$_POST['product_code'];
		}

		// check for sellType
		if ( isset( $_POST['selltype'] ) ) {
			// TODO: do we need to sanitize this?
			$selltype = (String)$_POST['selltype'];
		}

		// prepare data
		$properties = array();
		$allowed_properties = self::$allowed_shortcode_atts;

		// Check the "properties" -data-value
		if ( isset( $_POST['properties'] ) ) {

		    $raw_properties = $_POST['properties'];

	    	foreach (explode( ",", $raw_properties) as $property_str) {
	    		if ( strpos($property_str, '=') !== false ) {
	    			$property = explode( "=", $property_str );
	    			$property_name = str_replace(' ', '', $property[0]);
	    			if ( array_key_exists($property_name, $allowed_properties)){
	    				$property_value = str_replace(' ', '', $property[1]);
	    				$properties[$allowed_properties[$property_name]['original_name']] = self::sanitize_property_value( $property_value, $allowed_properties[$property_name]['type'] );
	    			}
	    		}else{
	    			$property_name = str_replace(' ', '', $property_str);
	    			if ( array_key_exists($property_name, $allowed_properties) ){
		    			$properties[$allowed_properties[$property_name]['original_name']] = '';
		    		}
	    		}
	    	}

		}

		// Check the individual -data-values for allowed properties
		foreach ($allowed_properties as $key => $value) {
			if ( isset($_POST[$key]) ) {
				$property_value = str_replace(' ', '', $value);
				$properties[$allowed_properties[$key]['original_name']] = self::sanitize_property_value( $_POST[$key], $value['type'] );
			}
		}

		$response = $this->cart_api_client->add_item_to_cart(
		    $product_code,
            $properties,
            $selltype,
        );

        $data = $this->parse_cart_api_response($response);
        if ( $data && $data['results'] == "success" ) {
            $result['added-to-cart'] = "true";
            if ($data['newItems']) {
            	$result['new-items'] = $data['newItems'];
            	// ["newItems"]=> array(1) { [0]=> int(539777) }
            }
            if ($data['summary']['count']) {
            	$result['items'] = $data['summary']['count'];
            }
        }
        if ( $data && $data['results'] == "error" ) {
            $result['error'] = $data;
        }
        echo json_encode($result);
    }


	/**
	 * AJAX get orderflow link
	 * Also go through the /mandatory/ page to check for mandatory selections
	 */
	public function lgpi_orderflow() {
		ob_start();
		$result = [
		    "url" => false,
		];
		$checkout_page_url = $this->get_checkout_url();
		if ( $checkout_page_url ) {
			$result["url"] = $checkout_page_url;
			if ( isset($_POST['new-items'])) {
				if ( isset( $_POST['new-items'][0] ) && is_numeric($_POST['new-items'][0]) ) {
					// add new item-ID to url
					$result["url"] .= "/mandatory/?item=".$_POST['new-items'][0];
					if ( isset( $_POST['new-items'][1] ) && is_numeric($_POST['new-items'][1]) ) {
						$result["url"] .= "&itemSecondary=".$_POST['new-items'][1];
					}
				}
			}
		}
        echo json_encode($result);
    }


	/**
	 * AJAX remove from cart.
	 */
	public function lgpi_remove_from_cart() {
		ob_start();
		$result = [
		    "removed-from-cart" => "false",
		];

		// itemid required
		if ( ! isset( $_POST['product_id'] ) ) {
			$result['warning'] = "product-is-missing";
			echo json_encode($result);
		}

		$response = $this->cart_api_client->remove_item_from_cart($_POST['product_id']);
        $data = $this->parse_cart_api_response($response);
        if ( $data && $data['results'] == "success" ) {
            $result['removed-from-cart'] = "true";
            // $result['product-id'] = $_POST['product_id'];
            if ($data['summary']['count']) {
            	$result['items'] = $data['summary']['count'];
            }
        }
        if ( $data && $data['results'] == "error" ) {
            $result['error'] = $data;
        }
        echo json_encode($result);
	}

    /**
     * @param $response
     * @return array|false
     */
    protected function parse_cart_api_response($response){
	    // check for nulls and fails...
	    if ( $response && is_array($response) && isset($response['json'])) {
	        $data = json_decode($response['json'], true);
	        // check for valid JSON
	        if($data !== null) {
	        	// check the returned JSON
	        	// if ( isset($data['user'] &&  $data['user'] !== null ) {
	        	// 	setcookie('lgpi_cart_login_name', $data['user']);
	        	// }else{
	        	// 	setcookie('lgpi_cart_login_name', null);
	        	// }

	        	// Workaround if cartId is missing from response, check summary instead. This is usually cart update
	        	if ( isset($data['cartId']) && !empty($data['cartId']) || isset($data['summary']) && !empty($data['summary']) ) {
	        		// get cart amount if it exists in the response
	        		if ($data['summary']['count']) {
	        			setcookie('lgpi_cart_amount', (int)$data['summary']['count'], 0, "/");
	        		}else{
	        			setcookie('lgpi_cart_amount', "", 0, "/");
	        		}
	        		$data['results'] = "success";
	        		return $data;
	        	}else if ( isset($data['login']) && !empty($data['login']) ) {
	        		$data['results'] = "success";
	        		return $data;
				}else{
	        		// expected data is missing
	        		return false;
	        	}
	        }
	    }else if( $response && is_array($response) && isset($response['error'])){
	    	// errors
	    	if( current_user_can( 'administrator' ) ){
	    		// include error if admin for easier debugging
	    		$data = json_decode($response['error'], true);
	        	$data['results'] = "error";
	    		return $data;
	    	}
	    	// trigger/log error?
	    	// if ( isset($response['error']) && !empty($response['error']) ) {
	    	// 	return $data;
	    	// }
	    }
	    // no meaningful data?
	    return false;
    }

	/**
	 * Sanitize property values for API
	 * @return $converted_value string|int|bool
	 * of proper type (string|int|bool)
	 */
	public static function sanitize_property_value($value, $type = 'string') {
		switch ($type) {
			case 'bool':

				if ($value == "true" || $value == "1") {
					$value_to_send = true;
				}else{
					$value_to_send = false;
				}

				$converted_value = (bool)$value_to_send;
				break;
			case 'int':
				$converted_value = (int)$value;
				break;
			case 'string':
				$converted_value = (string)$value;
				break;
			default:
				// default to string
				$converted_value = (string)$value;
				break;
		}
		return $converted_value;
	}


	/**
	 * AJAX update cart, returns the cart HTML
	 *
	 * @return string/HTML
	 */
	public function lgpi_update_cart() {
		ob_start();

		// TODO: make possible to update with JSON without making a new call

		$response = $this->cart_api_client->get_current_cart();
		$data = $this->parse_cart_api_response($response);

		$cart_notices_html = '';
		$cart_items_html = ''; // empty cart?
		$summary_html = '';

		if( $data ){ 

			if ( isset($data['error']) ) {
                $cart_notices_html .= '<div class="lgpi-message lgpi-error-message">'.$data['error'].'</div>';
			}else{
                $cart_notices_html .= '<div class="lgpi-message lgpi-success-message">'. __('Cart updated', 'LGPI') .'</div>';
            }

			if ( isset($data['summary']) ) {
				// HOOK: lgpi_cart_summary_data for manipulating cart_items
				$summary = apply_filters('lgpi_cart_summary_data', $data['summary']);
				// check count
				if ( isset($summary['currency']['symbol']) && isset($summary['currency']['name']) ) {
					$currency_symbol = ' '.__($summary['currency']['symbol'], 'LGPI');
					$currency_name = $summary['currency']['name'];
				}else{
					$currency_symbol = '';
					$currency_name = '';
				}

                if( isset($summary['count']) && $summary['count'] != 0 ) {
                    $summary_html = $this->generate_summary_html($summary, $currency_symbol);
                }
			}

            if ( isset($data['items']) ) {
                // HOOK: lgpi_cart_items_data for manipulating cart_items
                $cart_items = apply_filters('lgpi_cart_items_data', $data['items']);
                if( $cart_items ){
                    $cart_items_html = $this->generate_cart_items_html($summary['count'], $cart_items, $currency_symbol);
                }
            }else{
            	// if no items
            	setcookie('lgpi_cart_amount', "", 0, "/");
            }
		}else{
			// if no data
			setcookie('lgpi_cart_amount', "", 0, "/");
		}
		echo $this->lgpi_get_cart_html( $cart_items_html, $cart_notices_html, $summary_html );
	}


	/**
	 * AJAX update cart item num
	 *
	 * @return string/HTML
	 */
	public function lgpi_get_cart_item_count() {
		ob_start();
		$item_count = array("items" => 0);
		if ( isset( $_COOKIE['lgpi_cart_amount'] ) && is_integer( (int)$_COOKIE['lgpi_cart_amount'] ) && $_COOKIE['lgpi_cart_amount'] != 0 ) {
			$item_count["items"] = (int)$_COOKIE['lgpi_cart_amount'];
		}
		echo json_encode($item_count);
	}

	/**
	 * get cart html with included cart items, notices and summary
	 *
	 * @param string $fragments, string $notices, string $summary
	 * @return string/HTML
	 */
	public function lgpi_get_cart_html( $fragments = '', $notices = '', $summary = '' ) {
		$checkout_page_url = $this->get_checkout_url();
		$additional_classes = "";
		if ( empty( $fragments )) {
			$cart_empty = true;
			// add class for styling purposes
			$additional_classes .= " lgpi-cart_empty";
		}else{ $cart_empty = false; }
		$html = '<div class="lgpi_cart_html_inner_container'. $additional_classes .'">';
			$html .= '<div class="lgpi-notices-wrapper">'. $notices .'</div>';

			$cart_title = '<div class="lgpi-cart-top-title"><h2>'.__("Shopping Cart", "LGPI").'</h2></div>';
			$cart_title .= '<button class="lgpi_update_cart_button" type="button">'.__('Update cart', 'LGPI').'</button>';

			// HOOK: lgpi_cart_title_html for manipulating cart title html
			$cart_title = apply_filters('lgpi_cart_title_html', $cart_title);
			$html .= $cart_title;

			if ( $cart_empty ) {
				$empty_cart_html = '<div class="lgpi-cart-empty">'. __('Cart is empty', 'LGPI') .'</div>';
				// HOOK: lgpi_cart_empty_html for manipulating empty cart html
				$fragments = apply_filters('lgpi_cart_empty_html', $empty_cart_html);
			}else{
				// HOOK: lgpi_cart_fragments_html for manipulating fragments html
				$fragments = apply_filters('lgpi_cart_fragments_html', $fragments);
			}

			$html .= '<div class="lgpi-cart_contents">';

			$html .= $fragments;

			$html .= '</div>';
			$html .= '<div class="lgpi-cart_footer">';
				$html .= '<div class="lgpi-cart_summary">';

				$html .= $summary;

				$html .= '</div>';
				if ($checkout_page_url) {
					// Make the URL translateable for multilanguage compatibility
					$html .= '<a class="lgpi_proceed_to_checkout button cta" href="'.__($checkout_page_url, 'LGPI').'">'.__('Proceed to checkout', 'LGPI').'</a>';
				}
			$html .= '</div>'; /* end cart footer */
			$html .= '<div class="lgpi_ajax_checkout_fallback"><div class="lgpi-checkout-inner"></div></div>';
			$html .= '<span class="lgpi_loader_container"><span class="lgpi_loader"><span></span><span></span><span></span><span></span></span></span>';
		$html .= '</div>';

		return $html;
	}


	/**
	 * Print the initial CART HTML to be updated via AJAX
	 *
	 * @return string/HTML
	 */
	public function lgpi_print_frontend_cart(){
		// Initialize cart HTML.
		$html = '<div class="lgpi_cart_html_container" style="display:none">';
		$html .= '<div class="lgpi_cart_html_overlay"></div>';
		$html .= '<div id="lgpi_cart" class="lgpi_cart_html_abs_container">';
		// HOOK: lgpi_close_button_text for modifying the close button text html
		$close_button_text = __('X', 'LGPI');
		$html .= '<button class="lgpi_close_shopping_cart_button" title="'.__('Close shopping cart', 'LGPI').'">';
		$html .= apply_filters('lgpi_close_button_text', $close_button_text);
		$html .= '</button>';
		// HOOK: lgpi_before_inner_cart_container_html for adding html to cart
		$html .= apply_filters('lgpi_before_inner_cart_container_html', '');
		$html .= $this->lgpi_get_cart_html();
		// HOOK: lgpi_after__inner_cart_container_html for adding html to cart
		$html .= apply_filters('lgpi_after__inner_cart_container_html', '');
		$html .= '</div>';
		$html .= '</div>';
		echo $html;
	}

    /**
     * @param $summary
     * @param string $currency_symbol
     * @return string
     */
    protected function generate_summary_html($summary, string $currency_symbol): string
    {
        $summary_html = '<div class="lgpi-cart-summary-items">';
        // Summary for total price
        if (isset($summary["price"]) && isset($summary["price"]["vatExcluded"])) {
            $summary_html .= '<div class="lgpi-cart-summary-item">';
            $summary_html .= '<span class="lgpi-cart-summary-total-price lgpi_price_inner_container">';
            $summary_html .= '<span class="lgpi-cart-summary-total-price-vat-excluded lgpi_price_excluding_vat">';
	            $summary_html .= '<span class="lgpi-cart-summary-item-name lgpi-price-vat-excluded">' . __("Total price (excluding VAT)", "LGPI") . '</span>';
	            $summary_html .= '<span class="lgpi-cart-summary-item-value lgpi-price-vat-excluded-value">' . $summary["price"]["vatExcluded"] . $currency_symbol . '</span>';
            $summary_html .= '</span>';
            $summary_html .= '<span class="lgpi-cart-summary-total-price-vat-included lgpi_price_including_vat">';
	            $summary_html .= '<span class="lgpi-cart-summary-item-name lgpi-price-vat-included">' . __("Total price (including VAT)", "LGPI") . '</span>';
	            $summary_html .= '<span class="lgpi-cart-summary-item-value lgpi-price-vat-included-value">' . $this->calculate_price_with_vat($summary["price"]["vatExcluded"]) . $currency_symbol . '</span>';
            $summary_html .= '</span>';
            $summary_html .= '</span>';
            $summary_html .= '</div>';
        }
        $summary_html .= '</div>';
        return $summary_html;
    }

    /**
     * @param int $item_count
     * @param array $cart_items
     * @param string $currency_symbol
     * @return string
     */
    protected function generate_cart_items_html(int $item_count, array $cart_items, string $currency_symbol): string
    {
        $cart_items_html = '<div class="lgpi-cart-items">';
        $cart_items_html .= '<div class="lgpi-cart-items-count">' . __('Products in cart', "LGPI") . ': ' . $item_count . '</div>';

        $lgpi_remove_from_cart_button_text = 'x';
        // HOOK: lgpi_remove_from_cart_button_text for adding html to item remove from cart button
		$lgpi_remove_from_cart_button_text_inner = apply_filters('lgpi_remove_from_cart_button_text', $lgpi_remove_from_cart_button_text);

        foreach ($cart_items as $item) {

            // item properties
            $item_properties_html = '';
            if ($item["properties"]) {
                foreach ($item["properties"] as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $multi_property_key => $multi_property_value) {
                        	if ( !empty( $multi_property_value ) ) {
                            	$item_properties_html .= '<span class="lgpi-cart-item-property"><span class="lgpi-cart-item-property-name">' . ucfirst($multi_property_key) . '</span><span class="lgpi-cart-item-property-value">' . $multi_property_value . '</span></span>';
                        	}
                        }
                    } else {
                    	if ( !empty( $value ) ) {
	                        $item_properties_html .= '<span class="lgpi-cart-item-property"><span class="lgpi-cart-item-property-name">' . ucfirst($key) . '</span><span class="lgpi-cart-item-property-value">' . $value . '</span></span>';
	                    }
                    }
                }
            }
            // item options
            $item_options_html = '';
            if ($item["options"]) {
                foreach ($item["options"] as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $multi_option_key => $multi_option_value) {

                            if (is_array($multi_option_value)) {
                                $item_options_html .= '<ul class="lgpi-cart-item-option-group">';
                                foreach ($multi_option_value as $multiple_option_key => $multiple_option_value) {
                                    $item_options_html .= '<li class="lgpi-cart-item-option"><span class="lgpi-cart-item-option-name">' . ucfirst($multiple_option_key) . '</span><span class="lgpi-cart-item-option-value">' . $multiple_option_value . '</span></li>';
                                }
                                $item_options_html .= '</ul>';
                            } else {
                                // $item_options_html .= '<span class="lgpi-cart-item-option"><span class="lgpi-cart-item-option-name">'.$key.'</span><span class="lgpi-cart-item-option-value">'.$value.'</span></span>';
                                $item_options_html .= '<span class="lgpi-cart-item-option"><span class="lgpi-cart-item-option-name">' . ucfirst($multi_option_key) . '</span><span class="lgpi-cart-item-option-value">' . $multi_option_value . '</span></span>';
                            }

                        }
                    } else {
                        $item_options_html .= '<span class="lgpi-cart-item-option"><span class="lgpi-cart-item-option-name">' . ucfirst($key) . '</span><span class="lgpi-cart-item-option-value">' . $value . '</span></span>';
                    }
                }
            }

            $cart_items_html .= '<div class="lgpi-cart-item">';
            $cart_items_html .= '<span class="lgpi-cart-item-name">' . ucfirst($item["code"]) . '</span>';
            $cart_items_html .= '<span class="lgpi-cart-item-properties">' . $item_properties_html . '</span>';
            // Disable showing item options for now
            // $cart_items_html .= '<span class="lgpi-cart-item-options">'.$item_options_html.'</span>';
            $cart_items_html .= '<span class="lgpi-cart-item-price">';
	            $cart_items_html .= '<span class="lgpi_price_inner_container">';
	            if ( isset($item["price"]["pricePrefix"]) ) {
	            	$cart_items_html .= '<span class="lgpi-cart-item-price-prefix">' . $item["price"]["pricePrefix"] . '</span>';
	            }
	            if ( isset($item["price"]["vatExcluded"]) ) {
	            	$cart_items_html .= '<span class="lgpi-cart-item-price-vat-excluded lgpi_price_excluding_vat">' . $item["price"]["vatExcluded"] . '' . $currency_symbol . '</span>';
	            }
	            if ( isset($item["price"]["vatIncluded"]) ) {
	            	$cart_items_html .= '<span class="lgpi-cart-item-price-vat-included lgpi_price_including_vat">' . $item["price"]["vatIncluded"] . '' . $currency_symbol . '</span>';
	            }else{
	            	$cart_items_html .= '<span class="lgpi-cart-item-price-vat-included lgpi_price_including_vat">' . $this->calculate_price_with_vat( $item["price"]["vatExcluded"] ) . '' . $currency_symbol . '</span>';
	            }

	            // check for item payment period
	            if ( isset($item["period"]) ) {
	            	if ( $item["period"] > 1 ) {
	            		$item["price"]["priceSuffix"] = ' /' . $item["period"] . ' ' . __('months', 'LGPI');
	            	}else{
	            		$item["price"]["priceSuffix"] = ' /' . $item["period"] . ' ' . __('month', 'LGPI');
	            	}
	            }


	            if ( isset($item["price"]["priceSuffix"]) ) {
	            	$cart_items_html .= '<span class="lgpi-cart-item-price-suffix">' . $item["price"]["priceSuffix"] . '</span>';
	            }
	            $cart_items_html .= '</span>';
            $cart_items_html .= '</span>';
            $cart_items_html .= '<button class="lgpi_cart_item_remove_button lgpi_ajax_remove_from_cart" data-product_code="' . $item["code"] . '" data-product_id="' . $item["id"] . '" title="' . __("Remove product from cart", "LGPI") . '">'.$lgpi_remove_from_cart_button_text_inner.'</button>';
            $cart_items_html .= '<span class="lgpi_loader_container"><span class="lgpi_loader"><span></span><span></span><span></span><span></span></span></span>';
            $cart_items_html .= '</div>';
        }
        $cart_items_html .= '</div>';
        return $cart_items_html;
    }

    /**
     * used to calculate price including VAT%
     * @param string/float/int $amount
     * @return float/bool
     */
    public function calculate_price_with_vat( $amount = false )
    {
    	$vat = $this->lgpi_settings_cache->getSettingsData()[ 'lgpi_vat_amount' ]->getValue();
        if ( $amount !== false && $vat !== false ) {
        	$amount_with_vat = round( ((float)$amount * (float)$vat)*100, 3);
        	return ceil( $amount_with_vat )/100;
        }
        return $amount;
    }

    /**
     * get product object
     * @param int $post_id
     * @return object/bool
     */
    public function get_product( $id = false )
    {
    	$product = false;

    	if ( $id !== false && is_integer((int)$id) ) {
			// Default args
			$args = array(
				'post_type' => 'lgpi-product', 
				'posts_per_page'=>1, 
				'post_status' => 'publish', 
				'orderby' => 'menu_order', 
				'order' => 'DESC', 
				'suppress_filters'=> 'false', 
				'include' => array($id)
			);

			// Get posts
		    $product_objects = get_posts($args);

		    if ( !$product_objects[0] ) {
		    	return false;
		    }

		    // set first
		    $product = $product_objects[0];
    	}
        return $product;
    }

    /**
     * used to get specified currency symbol
     * @return string
     */
    public function get_currency_symbol(): string
    {
    	$symbol = $this->lgpi_settings_cache->getSettingsData()[ 'lgpi_currency_symbol' ]->getValue();
        if ( $symbol ) {
        	return $symbol;
        }
        return "";
    }
}
