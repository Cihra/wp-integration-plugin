<?php
/**
 * Admin View: Settings
 *
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="wrap lgpi-container">
	<form id="lgpi_mainform" method="post" action="options.php">
		<!-- <nav class="nav-tab-wrapper"> -->
			<!-- nav? -->
		<!-- </nav> -->
		<h1><?php _e('Loopia Platform settings', 'LGPI'); ?></h1>
		<?php
			settings_fields( 'lgpi_settings' );
			do_settings_sections( 'lgpi_settings' );
			$settings = $GLOBALS['LGPI']->get_settings();
			$version = LGPI_VERSION;
		?>
		<table class="form-table">
	 		<tr valign="top">
		 		<th scope="row">
		 			<?php _e('Plugin version', 'LGPI'); ?>
		 		</th>
		 		<td>
		 			<?php echo $version; ?>
		 		</td>
	 		</tr>
	 		<tr valign="top">
		 		<th scope="row">
		 			<p><?php _e('Usable shortcodes', 'LGPI'); ?></p>
		 		</th>
		 		<td>
		 			<code>[lgpi-cart]</code> <?php _e('to print button that opens the shopping cart','LGPI'); ?> <br>
		 			<code>[lgpi-add-to-cart product="product-code"]</code> <?php _e('to print button that adds specified product to the shopping cart','LGPI'); ?> <br>
		 			<code>[lgpi-checkout]</code> <?php _e('to print the embedded checkout/orderflow','LGPI'); ?> <br>
		 			<code>[lgpi-login]</code> <?php _e('to print a link to the /login endpoint','LGPI'); ?> <br>
		 			<code>[lgpi-price id="data-id" prefix="From" suffix="/month"]</code> <?php _e('to print product price with and without VAT','LGPI'); ?> <br>
		 			<code>[lgpi-vat-toggle prefix="All prices have" suffix="" included_text="VAT included" excluded_text="VAT Excluded"]</code> <?php _e('to print VAT toggle switch (prices including/excluding VAT)','LGPI'); ?> <br>
		 			
		 			<p><i><?php _e('Make sure you enter the required settings below before using the shortcodes', 'LGPI'); ?></i></p>
		 		</td>
	 		</tr>
			<?php
            /**
             * @var LgpiSettings $properties
             */
				foreach ($settings as $setting => $properties) {
					if ($setting) {
						$setting_value = '';
						$setting_title = $properties->getTitle() ?? $setting;
						$setting_type = $properties->getType() ?? '';
						$setting_text = $properties->getText() ?? '';
						$save_as_option = $properties->getSaveAsOptions() ?? false;
						if($save_as_option){
							$setting_value = esc_attr( get_option($setting) );
						}

						switch ($setting_type) {
							case 'heading':
						 		?>
						 		<tr valign="top">
							 		<th scope="row" colspan="2">
							 			<h2><?php _e($setting_title, 'LGPI'); ?></h2>
							 			<?php if ($setting_text): ?>
							 				<p><?php _e($setting_text, 'LGPI'); ?></p>
							 			<?php endif ?>
							 		</th>
						 		</tr>
								<?php
								break;

							case 'info':
						 		?>
						 		<tr valign="top">
							 		<th scope="row">
							 			<p><?php _e($setting_title, 'LGPI'); ?></p>
							 		</th>
							 		<td>
							 			<?php if ($setting_text): ?>
							 				<p><?php _e($setting_text, 'LGPI'); ?></p>
							 			<?php endif ?>
							 		</td>
						 		</tr>
								<?php
								break;

							case 'text':
						 		?>
						 		<tr valign="top">
							 		<th scope="row">
							 			<?php _e($setting_title, 'LGPI'); ?>
							 		</th>
							 		<td>
							 			<?php if ($setting_text): ?>
							 				<p><?php _e($setting_text, 'LGPI'); ?></p>
							 			<?php endif ?>
							 			<input style="width:400px;" type="text" name="<?php if( !empty($setting) ){ echo $setting; } ?>" id="<?php if( !empty($setting) ){ echo $setting; } ?>" value="<?php if( !empty($setting_value) ){ echo $setting_value; } ?>">
							 		</td>
						 		</tr>
								<?php
								break;

							case 'check':
						 		?>
						 		<tr valign="top">
							 		<th scope="row">
							 			<?php _e($setting_title, 'LGPI'); ?>
							 		</th>
							 		<td>
							 			<?php if ($setting_text): ?>
							 				<p><?php _e($setting_text, 'LGPI'); ?></p>
							 			<?php endif ?>

							 			<select name="<?php if( !empty($setting) ){ echo $setting; } ?>" id="<?php if( !empty($setting) ){ echo $setting; } ?>">
							 				<option value="0" <?php if( $setting_value == '0' ){ echo "selected"; } ?> ><?php _e('False', 'LGPI') ?></option>
							 				<option value="1" <?php if( $setting_value == '1' ){ echo "selected"; } ?> ><?php _e('True', 'LGPI') ?></option>
							 			</select>
							 		</td>
						 		</tr>
								<?php
								break;

							default:
						 		?>
						 		<tr valign="top">
							 		<th scope="row">
							 			<?php _e($setting_title, 'LGPI'); ?>
							 		</th>
							 		<td>
							 			<?php if ($setting_text): ?>
							 				<p><?php _e($setting_text, 'LGPI'); ?></p>
							 			<?php endif ?>
							 			<input style="width:400px;" type="text" name="<?php if( !empty($setting) ){ echo $setting; } ?>" id="<?php if( !empty($setting) ){ echo $setting; } ?>" value="<?php if( !empty($setting_value) ){ echo $setting_value; } ?>">
							 		</td>
						 		</tr>
								<?php
								break;
						}
					}
				}
			 ?>
		</table>
		<p class="submit">
			<button name="save" class="lgpi-save-button" type="submit" value="<?php esc_html_e( 'Save changes', 'LGPI' ); ?>"><?php esc_html_e( 'Save changes', 'LGPI' ); ?></button>
		</p>
	</form>
</div>
