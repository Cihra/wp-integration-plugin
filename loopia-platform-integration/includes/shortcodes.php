<?php
/**
 * Shortcodes
 */

defined( 'ABSPATH' ) || exit;

add_shortcode('lgpi-cart', function($atts) {

    // Shortcode attributes
    $attributes = shortcode_atts( array(
    	'button_text' => '',
    	'text' => '',
    	'title' => '',
        'product' => '',
        'classes' => '',
        'before' => '',
        'after' => '',
    ), $atts );

    $before = '<p class="lgpi_toggle_cart_button_container">';
    $after = '</p>';
    $button_text = __('Cart', 'LGPI');
    $css_classes = "link-button";
    $title = __('Open shopping cart', 'LGPI');

    // override attributes
    if( $attributes['title'] ){ $title = esc_html( $attributes['title'] ); }
    if( $attributes['before'] ){ $before = $attributes['before']; }
    if( $attributes['after'] ){ $after = $attributes['after']; }
    if( $attributes['button_text'] ){ $button_text = $attributes['button_text']; }
    if( $attributes['text'] ){ $button_text = $attributes['text']; }
    if( $attributes['classes'] ){ $css_classes = esc_html( $attributes['classes'] ); }

    // Load cart scripts --> These are loaded in the plugin already.
    // wp_enqueue_script("lgpi_cart_js");

    ob_start();
    echo $before;
    ?><button class="lgpi_cart_toggle_button <?php echo $css_classes; ?>" aria-expanded="false" title="<?php echo $title; ?>" aria-label="<?php echo $title; ?>"><?php echo $button_text; ?><span class="lgpi_cart_button_amount_container"></span></button><?php
    echo $after;
    return ob_get_clean();
});

add_shortcode('lgpi-add-to-cart', function($atts) {

	$allowed_shortcode_atts = LoopiaPlatform::get_allowed_shortcode_atts();

	$shortcode_atts_array = array(
        'button_text' => '',
        'text' => '',
        'title' => '',
        'product' => '',
        'properties' => '',
        'classes' => '',
        'before' => '',
        'after' => '',
        'id' => '',
    );

    foreach ($allowed_shortcode_atts as $key => $value) {
    	$shortcode_atts_array[$key] = '';
    }

    // Shortcode attributes
    $attributes = shortcode_atts( $shortcode_atts_array, $atts );

    $before = '<p class="lgpi_add_to_cart_button_container">';
    $after = '</p>';
    $button_text = __('Add to Cart', "LGPI");
    $product_code = "";
    $css_classes = "link-button";
    $properties_prepared = '';
    $singular_properties_prepared = '';
    $id = '';

    // override attributes
    if( $attributes['title'] ){ $title = esc_html( $attributes['title'] ); }
    if( $attributes['product'] ){ $product_code = esc_html( $attributes['product'] ); }
    if( $attributes['before'] ){ $before = $attributes['before']; }
    if( $attributes['after'] ){ $after = $attributes['after']; }
    if( $attributes['button_text'] ){ $button_text = $attributes['button_text']; }
    if( $attributes['text'] ){ $button_text = $attributes['text']; }
    if( $attributes['classes'] ){ $css_classes = esc_html( $attributes['classes'] ); }
    if( $attributes['id'] ){ $id = 'id="'.esc_html( $attributes['id'] ).'"'; }

    if ( empty($product_code) ) {
    	// check for product code -> this is required
    	return false;
    }

    if( $attributes['properties'] ){
	    // $raw_properties = esc_html( $attributes['properties'] );
    	$properties_prepared = str_replace(' ', '', esc_html( $attributes['properties'] ));
	}

	// Create individual data-properties
	foreach ($attributes as $key => $value) {
		if ( array_key_exists($key, $allowed_shortcode_atts) && !empty($value) ) {
			$singular_properties_prepared .= ' data-' . $key . '="' . esc_html( $value ) . '"';
		}
	}

    // Load cart scripts --> These are loaded in the plugin already.
    // wp_enqueue_script("lgpi_cart_js");

    ob_start();
    echo $before;
    ?><button data-product_code="<?php echo $product_code; ?>" data-properties="<?php echo $properties_prepared; ?>" <?php echo $singular_properties_prepared; ?> <?php echo $id; ?> class="lgpi_add_to_cart button lgpi_ajax_add_to_cart <?php echo $css_classes; ?>" <?php if( !empty( $title )){ echo 'title="'.$title.'"'; echo 'aria-label="'.$title.'"'; } ?>> <?php echo $button_text; ?> <span class="lgpi_add_to_cart_button_status"></span><span class="lgpi_loader_container"><span class="lgpi_loader"><span></span><span></span><span></span><span></span></span></span></button><?php
    echo $after;
    return ob_get_clean();
});

add_shortcode('lgpi-checkout', function($atts) {

	// Shortcode attributes
	$attributes = shortcode_atts( array(
	    'lang' => '',
	    'language' => '',
	    'override_currency' => '',
	), $atts );

	// Only initialize the checkout assets when needed..
	if ( $GLOBALS['LGPI']->init_checkout_assets() === false ) {
	    /*
		Load main css & js files. These files should be parsed from
		manifest.json, published by client. You need to include these files:
		app.js, chunk-vendors.js, app.css, chunk-vendors.css. You can find
		their real path and names including hash in published manifest.json. Please
		do cache them for better performance. If they change, the hash will change
		accordingly, this mechanism must invalidate cached version of the files.
	    */

		// If this fails, then we dont have the required assets available..
		return false;
	}

	// Enque required scripts and styles
	add_action( 'wp_enqueue_scripts', $GLOBALS['LGPI']->enqueue_spa_scripts(), 13 );
	add_action( 'wp_enqueue_scripts', $GLOBALS['LGPI']->enqueue_spa_styles(), 15 );

	$lang = 'en-US';
	$override_currency = false;

	if( $attributes['lang'] ){ $lang = esc_html( $attributes['lang'] ); }
	if( $attributes['language'] ){ $lang = esc_html( $attributes['language'] ); }
	// Ability to override option by giving currency to the shortcode
	if( !empty($attributes['override_currency']) ){ $override_currency = esc_html( $attributes['override_currency'] ); }

    // Check required settings!
    $checkout_parameters = $GLOBALS['LGPI']->get_lgpi_options('checkout');

    if ( $checkout_parameters && $lang) { $checkout_parameters["language"] = $lang; }
    if ( $checkout_parameters && $override_currency) {
    	// Ability to override option by giving currency to the shortcode
	    $checkout_parameters["currency"] = $override_currency;
	}
	
    if ($checkout_parameters) {
		$checkout_parameters_json = json_encode($checkout_parameters);
    	ob_start();
	    ?><div id="lgpi_checkout_container" class="lgpi-checkout"><div class="lgpi-checkout-inner"><div id="main-content" data-params='<?php echo $checkout_parameters_json; ?>'></div></div></div><?php
	    return ob_get_clean();
    }
    return false;
});

add_shortcode('lgpi-login', function($atts) {

    // Shortcode attributes
    $attributes = shortcode_atts( array(
    	'button_text' => '',
    	'logout_button_text' => '',
    	'text' => '',
    	'logout_text' => '',
    	'title' => '',
    	'logout_title' => '',
    	'dropdown' => '',
    	'mobilenav' => '',
    	'classes' => '',
        'before' => '',
        'after' => '',
        'login_override' => NULL,
        'login_redir_override' => NULL,
        'login_redir_current_page' => NULL,
    ), $atts );

    $before = '<div class="lgpi_login_button_container">';
    $after = '</div>';
    $button_text = __('Login', 'LGPI');
    $logout_button_text = __('Logout', 'LGPI');
    $title = __('Login', 'LGPI');
    $logout_title = __('Logout', 'LGPI');
    $dropdown_menu_title = __('Open submenu', 'LGPI');
    $css_classes = "link-button";
    $login_redir_current_page = false;
    $mode = "default";
    $lgpi_cart_api_url = $GLOBALS['LGPI']->lgpi_settings_cache->getSettingsData()[ 'lgpi_cart_api_url' ]->getValue();

    // we need API URL to continue
    if ( !$lgpi_cart_api_url ) {
    	return false;
    }

    // override attributes
    if( $attributes['title'] ){ $title = esc_html( $attributes['title'] ); }
    if( $attributes['logout_title'] ){ $logout_title = esc_html( $attributes['logout_title'] ); }
    if( $attributes['before'] ){ $before = $attributes['before']; }
    if( $attributes['after'] ){ $after = $attributes['after']; }
    if( $attributes['button_text'] ){ $button_text = $attributes['button_text']; }
    if( $attributes['text'] ){ $button_text = $attributes['text']; }
    if( $attributes['logout_button_text'] ){ $logout_button_text = $attributes['logout_button_text']; }
    if( $attributes['logout_text'] ){ $logout_button_text = $attributes['logout_text']; }
    if( $attributes['classes'] ){ $css_classes = esc_html( $attributes['classes'] ); }
    if( $attributes['dropdown'] == 1 || $attributes['dropdown'] == "1" ){ $mode = "dropdown"; }
    if( $attributes['mobilenav'] == 1 || $attributes['mobilenav'] == "1" ){ $mode = "mobilenav"; }
    if( $attributes['login_redir_current_page'] == 1 || $attributes['login_redir_current_page'] == "1" ){ $login_redir_current_page = true; }

    $lgpi_logout_button_url = $lgpi_cart_api_url . '/logout';
    if( $attributes['login_redir_override'] !== NULL ){  
    	$redirect_url = esc_html( $attributes['login_redir_override'] );
    }else{
    	$redirect_url = $GLOBALS['LGPI']->get_checkout_url();
    }
    if ($login_redir_current_page && get_permalink()) {
		$redirect_url = get_permalink();
	}
    $lgpi_redirect_after_login = '?redirectAfterLogin=' . urlencode($redirect_url) . '';
    if( $attributes['login_override'] !== NULL ){ 
    	$lgpi_login_button_url = esc_html( $attributes['login_override'] ); 
	}else{
    	$lgpi_login_button_url = $lgpi_cart_api_url . '/login' . $lgpi_redirect_after_login;
	}
    $logout_button_html = '<a style="display:none;" href="'.$lgpi_logout_button_url.'" class="lgpi_login_link lgpi_logout_button_link '.$css_classes.'" title="'.$logout_title.'" aria-label="'.$logout_title.'">'.$logout_button_text.'<span class="lgpi_loader_container"><span class="lgpi_loader"><span></span><span></span><span></span><span></span></span></span></a>';
    $html = '';

    switch ($mode) {
    	case 'mobilenav':
	    	$html .= '<span style="display:none;" class="lgpi_login_username"></span>';
	    	$html .= '<div class="lgpi_login_links_container">';
		    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_account"></div>';
		    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_login_services"></div>';
		    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_login_invoices"></div>';
		    	$html .= '<div class="lgpi_user_dropdown_item lgpi_logout_item">'.$logout_button_html.'</div>';
	    	$html .= '</div>';
    		break;

    	case 'dropdown':
	    	$html .= '<div class="lgpi_login_dropdown_container">';
	    	$html .= '<span style="display:none;" class="lgpi_login_username"></span>';
	    	$html .= '<button style="display:none;" class="lgpi_open_dropdown_content disable-button-styles element-invisible element-focusable" aria-expanded="false" aria-label="'.$dropdown_menu_title.'" title="'.$dropdown_menu_title.'"><i class="dropdown-icon far fa-angle-down"></i></button>';
		    	$html .= '<div class="lgpi_dropdown_content" style="display:none" aria-expanded="false">';
			    	$html .= '<div class="lgpi_user_dropdown_item lgpi_login_username"></div>';
			    	$html .= '<div class="lgpi_user_dropdown_item lgpi_login_email"></div>';
			    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_account"></div>';
			    	$html .= '<div class="lgpi_user_dropdown_item lgpi_dropdown_separator"></div>';
			    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_login_services"></div>';
			    	$html .= '<div style="display:none;" class="lgpi_user_dropdown_item lgpi_login_invoices"></div>';
			    	$html .= '<div class="lgpi_user_dropdown_item lgpi_logout_item">'.$logout_button_html.'</div>';
		    	$html .= '</div>';
	    	$html .= '</div>';
    		break;
    	
    	default:
    		// default || no dropdown
    		$html .= '<span style="display:none;" class="lgpi_login_username"></span>';
    		$html .= $logout_button_html;
    		break;
    }

	ob_start();
    echo $before;
    ?><div class="lgpi_user_details_container processing"><?php echo $html; ?><a href="<?php echo $lgpi_login_button_url; ?>" class="lgpi_login_link lgpi_login_button_link <?php echo $css_classes; ?>" title="<?php echo $title; ?>" aria-label="<?php echo $title; ?>"><?php echo $button_text; ?></a><span class="lgpi_loader_container"><span class="lgpi_loader"><span></span><span></span><span></span><span></span></span></span></div><?php
    echo $after;
    return ob_get_clean();
});

add_shortcode('lgpi-price', function($atts) {

	$attributes = shortcode_atts(array(
	        'prefix' => '',
	        'suffix' => '',
	        'excluded_title' => '',
	        'included_title' => '',
	        'campaign' => '',
	        'id' => '',
	        'classes' => '',
	        'before' => '',
	        'after' => '',
	    ), $atts);

	    $before = '<p class="lgpi_price_outer_container">';
	    $after = '</p>';
	    $id = "";
	    $css_classes = "";
	    $prefix = "";
	    $suffix = "";
	    $campaign = "";
	    $excluded_title = __('VAT excluded', 'LGPI');
	    $included_title = __('VAT included', 'LGPI');

	    // override attributes
	    if( $attributes['excluded_title'] ){ $excluded_title = esc_html( $attributes['excluded_title'] ); }
	    if( $attributes['included_title'] ){ $included_title = esc_html( $attributes['included_title'] ); }
	    if( $attributes['campaign'] ){ $campaign = esc_html( $attributes['campaign'] ); }
	    if( $attributes['id'] ){ $id = esc_html( $attributes['id'] ); }
	    if( $attributes['before'] ){ $before = $attributes['before']; }
	    if( $attributes['after'] ){ $after = $attributes['after']; }
	    if( $attributes['prefix'] ){ $prefix = '<span class="lgpi_price_additional_prefix">'.$attributes['prefix'].'</span>'; }
	    if( $attributes['suffix'] ){ $suffix = '<span class="lgpi_price_additional_suffix">'.$attributes['suffix'].'</span>'; }
	    if( $attributes['classes'] ){ $css_classes = esc_html( $attributes['classes'] ); }

		if( empty($id) ){
			return false;
			// exit early if no id
		}

		// Get product
	    $product = $GLOBALS['LGPI']->get_product($id);

	    if ( $product === false ) {
	    	return false;
	    }

		$currency_symbol = __($GLOBALS['LGPI']->get_currency_symbol(), 'LGPI');

	    // regular prices
		$price_excluding_vat = $product->lgpi_price_vat_excluded;
		$price_including_vat = $GLOBALS['LGPI']->calculate_price_with_vat($price_excluding_vat);


	    // SALE prices
		$alternative_campaign_active = false;
		$campaign_active = false;
		$campaign_class = "";
		$sale_price_excluding_vat = $product->lgpi_sale_price_vat_excluded;
		if ( $sale_price_excluding_vat !== '' ) {
			$sale_price_including_vat = $GLOBALS['LGPI']->calculate_price_with_vat($sale_price_excluding_vat);
			if ( !empty($campaign) ) {
				$alternative_campaign_active = true;
				// alternative format: Price is ___ and after X, it will be this ___
				$campaign_class = " alternative_campaign_active";
			}else{
				$campaign_active = true;
				// regular sale price
				$campaign_class = " campaign_active";
			}
		}
		// $price_note = $product->lgpi_price_vat_note;

	    ob_start();
	    echo $before;
	    ?>
		    <span
		    data-product_id="<?php echo $id; ?>"
		    class="lgpi_price_inner_container <?php echo $css_classes; echo $campaign_class; ?>">

    		   	<?php echo $prefix; ?>

				<?php if( $alternative_campaign_active ): /* Alternative formatting */  ?>
				    <span class="lgpi_price_field lgpi_price_excluding_vat" <?php if( !empty( $excluded_title )){ echo 'title="'.$excluded_title.'"'; } ?>>
				    	<span class="lgpi_price_amount">
				    		<span class="lgpi_price_campaign"><?php echo $sale_price_excluding_vat; ?></span>
				   		</span>
			   		 	<span class="lgpi_price_currency">
			   		 		<?php echo $currency_symbol; ?>
			   			</span>
			   			<?php // echo $suffix; ?>
			   		 	<span class="lgpi_price_campaign_text">
			   		 		<?php echo $campaign; ?>
			   		 	</span>
			   		 	<span class="lgpi_price_amount">
			   		 		<span class="lgpi_price_regular"><?php echo $price_excluding_vat; ?></span>
			   			</span>
				    	<span class="lgpi_price_currency">
				    		<?php echo $currency_symbol; ?>
				   		</span>
				   		<?php echo $suffix; ?>
				    </span>
				    <span class="lgpi_price_field lgpi_price_including_vat" <?php if( !empty( $included_title )){ echo 'title="'.$included_title.'"'; } ?>>
	 			    	<span class="lgpi_price_amount">
	 			    		<span class="lgpi_price_campaign"><?php echo $sale_price_including_vat; ?></span>
	 			   		</span>
	 		   		 	<span class="lgpi_price_currency">
	 		   		 		<?php echo $currency_symbol; ?>
	 		   			</span>
	 		   			<?php // echo $suffix; ?>
	 		   		 	<span class="lgpi_price_campaign_text">
	 		   		 		<?php echo $campaign; ?>
	 		   		 	</span>
	 		   		 	<span class="lgpi_price_amount">
	 		   		 		<span class="lgpi_price_regular"><?php echo $price_including_vat; ?></span>
	 		   			</span>
	 			    	<span class="lgpi_price_currency">
	 			    		<?php echo $currency_symbol; ?>
	 			   		</span>
	 			   		<?php echo $suffix; ?>
				    </span>
    		    <?php else:  /* Regular pricing / sale price */ ?>
	    		    <span class="lgpi_price_field lgpi_price_excluding_vat" <?php if( !empty( $excluded_title )){ echo 'title="'.$excluded_title.'"'; } ?>>
	    		    	<span class="lgpi_price_amount">
	    		    		<span class="lgpi_price_regular"><?php echo $price_excluding_vat; ?></span>
	    		    		<?php if( $campaign_active ): ?>
	    		    		<span class="lgpi_price_campaign"><?php echo $sale_price_excluding_vat; ?></span>
	    		    		<?php endif; ?>
	    		   		</span>
	    		    	<span class="lgpi_price_currency">
	    		    		<?php echo $currency_symbol; ?>
	    		   		</span>
	    		   		<?php echo $suffix; ?>
	    		    </span>
	    		    <span class="lgpi_price_field lgpi_price_including_vat" <?php if( !empty( $included_title )){ echo 'title="'.$included_title.'"'; } ?>>
	    	    	 	<span class="lgpi_price_amount">
	    	    	 		<span class="lgpi_price_regular"><?php echo $price_including_vat; ?></span>
	    		    		<?php if( $campaign_active ): ?>
	    	    	 		<span class="lgpi_price_campaign"><?php echo $sale_price_including_vat; ?></span>
	    	    	 		<?php endif; ?>
	    	    		</span>
	    	    	 	<span class="lgpi_price_currency">
	    	    	 		<?php echo $currency_symbol; ?>
	    	    		</span>
    		    		<?php echo $suffix; ?>
	    		    </span>
    		    <?php endif; ?>


		    </span>

	    <?php
	    echo $after;
	    return ob_get_clean();
});

add_shortcode('lgpi-vat-toggle', function($atts) {

	$attributes = shortcode_atts(array(
		'prefix' => '',
		'suffix' => '',
        'text' => '',
        'included_text' => '',
        'excluded_text' => '',
        'title' => '',
        'classes' => '',
        'before' => '',
        'after' => '',
    ), $atts);

    $before = '<p class="lgpi_vat_toggle_outer_container">';
    $after = '</p>';
    $css_classes = "";
    $text = "";
    $excluded_text = __('VAT excluded', 'LGPI');
    $included_text = __('VAT included', 'LGPI');
    $prefix = "";
    $suffix = "";

    // override attributes
    if( $attributes['title'] ){ $title = esc_html( $attributes['title'] ); }
    if( $attributes['before'] ){ $before = $attributes['before']; }
    if( $attributes['after'] ){ $after = $attributes['after']; }
    if( $attributes['text'] ){ $text = '<span class="lgpi_price_additional_text">'.$attributes['text'].'</span>'; }
    if( $attributes['classes'] ){ $css_classes = esc_html( $attributes['classes'] ); }
    if( $attributes['excluded_text'] ){ $excluded_text = esc_html( $attributes['excluded_text'] ); }
    if( $attributes['included_text'] ){ $included_text = esc_html( $attributes['included_text'] ); }
    if( $attributes['prefix'] ){ $prefix = '<span class="lgpi_vat_toggle_additional_prefix">'.$attributes['prefix'].'</span>'; }
    if( $attributes['suffix'] ){ $suffix = '<span class="lgpi_vat_toggle_additional_suffix">'.$attributes['suffix'].'</span>'; }

    ob_start();
	echo $before;
	?><span class="lgpi_vat_toggle_inner_container <?php echo $css_classes; ?>"><?php echo $prefix; ?><span class="lgpi_vat_toggle_button_container"><button class="toggle_vat_status lgpi_vat_excluded"> <span class="lgpi_vat_indicator"></span> <?php echo $excluded_text; ?></button><button class="toggle_vat_status lgpi_vat_included"> <span class="lgpi_vat_indicator"></span> <?php echo $included_text; ?></button></span><?php echo $suffix; ?></span><?php echo $after;
	return ob_get_clean();
});