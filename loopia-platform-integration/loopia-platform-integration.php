<?php
/**
 * Plugin Name: Loopia Platform integration
 * Version: 0.8.8
 * Description: Loopia platform toolkit
 *
 * textdomain for translations = LGPI
 *
 */

require( dirname( __FILE__ ) . '/vendor/autoload.php');

defined( 'ABSPATH' ) || exit;

if ( ! defined( 'LGPI_PLUGIN_FILE' ) ) {
	define( 'LGPI_PLUGIN_FILE', __FILE__ );
}

if ( ! class_exists( 'LoopiaPlatform', false ) ) {
	include_once dirname( __FILE__ ) . '/includes/class-loopiaplatform.php';
}

function LGPI_instance() {
	return LoopiaPlatform::instance();
}
$GLOBALS['LGPI'] = LGPI_instance();
