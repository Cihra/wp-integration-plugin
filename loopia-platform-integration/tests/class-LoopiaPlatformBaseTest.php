<?php
/**
 * Class with helful methods to be used in test classes
 * All class extend this class
 */
class LoopiaPlatformBaseTest extends WP_UnitTestCase {
    const API_URL = 'https://api-cart.staging.websupport.se/';
    const ASSETS_URL = 'https://spa-cart.staging.websupport.se';
    const COOKIE_NAME = 'cart_session';
    const COOKIE_VALUE = 'ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa';
    const COOKIE_STRING = self::COOKIE_NAME.'='.self::COOKIE_VALUE.'; expires=Sun, 12-Mar-2023 13:02:59 GMT; Max-Age=77760000; path=/; domain=.websupport.se';

    protected static function debug($myDebugVar) {
        fwrite(STDERR, print_r($myDebugVar, TRUE));
    }

    /**
     * @param string $class
     * @param string $name
     * @return ReflectionMethod
     * @throws ReflectionException
     */
    protected static function getMethod($class,$name) {
        $class = new ReflectionClass($class);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @param string $className
     * @param array $methods
     * @param array $constructorArgs
     * @return mixed
     */
    protected function getMock($className, $methods = [], $constructorArgs = [])
    {
        return $this->getMockBuilder($className)
            ->setConstructorArgs($constructorArgs)
            ->setMethods($methods)
            ->getMock();
    }

    protected static function remove_cookie()
    {
        if (isset($_COOKIE['cart_session'])) {
            unset($_COOKIE['cart_session']);
            //setcookie('cart_session', null, -1, '/');
        }
    }

    /**
     * Dummy test to avoid warnings
     */
    public function test_dummy()
    {
        $this->assertTrue(true);
    }
}
