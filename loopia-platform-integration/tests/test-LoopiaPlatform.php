<?php
/**
 * Class LoopiaPlatformTest
 *
 * @package Loopia_Platform_Integration
 */

include_once 'class-LoopiaPlatformBaseTest.php';

/**
 * Sample test case.
 */
class LoopiaPlatformTest extends LoopiaPlatformBaseTest {

    /**
     * @var LoopiaPlatform
     */
    private $instance;

    /**
     * This function will be executed before each test.
     */
    public function setUp(){
        parent::setup();
        $this->instance = LoopiaPlatform::instance();

        //initializing cache
        $settingsCache = new LgpiSettingsCache();
        $settingsCache->setSettingsData([
            'lgpi_cart_api_url' => new LgpiSetting('Cart API url','text', true, self::API_URL ),
            'lgpi_ssl_disabled' => new LgpiSetting('Disable SSL peer verification','check', true, false ),
            'lgpi_vat_amount' => new LgpiSetting('Vat amount','text', true, "1.25" )
        ]);
        $this->instance->setLgpiSettingsCache($settingsCache);

        //reinitializing API client
        $this->instance->init_cart_api_client();
    }
	/**
	 * A single example test.
	 */
	public function test_LoopiaPlatform_constructor() {
        $mockObject = $this->getMock(get_class($this->instance),[
            'define_constants',
            'init_settings',
            'includes',
            'init_cart_api_client',
            'init_hooks',
            'register_settings'
        ] );

        $mockObject->expects($this->once())->method('define_constants');
        $mockObject->expects($this->once())->method('init_settings');
        $mockObject->expects($this->once())->method('includes');
        $mockObject->expects($this->once())->method('init_hooks');
        $mockObject->expects($this->once())->method('init_cart_api_client');
        $mockObject->expects($this->once())->method('register_settings');

	    $mockObject->__construct();
	}

    /**
     */
    public function test_lgpi_user_details_loggedin_false()
    {
        $this->instance->lgpi_user_details();
        $output = ob_get_clean();
        //$this->assertTrue('{"logged-in":"false","user-data":[]}' == $output);
        $this->assertEquals('{"logged-in":"false","user-data":[]}', $output);
    }

    public function test_lgpi_user_details_loggedin_true()
    {
        $_COOKIE['cart_session'] = 'ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa';
        $this->instance->lgpi_user_details();
        $output = ob_get_clean();
        $this->assertEquals('{"logged-in":"false","user-data":[]}' , $output);
    }

	public function est_LoopiaPlatform_init()
    {
        $GLOBALS['pagenow'] = 'wp-login.php';

        $this->assertTrue( wp_style_is( 'lgpi_frontend', 'registered' ) );
        $this->assertTrue( wp_script_is( 'lgpi_cart_js', 'registered' ) );
        $this->assertFalse( wp_style_is( 'lgpi_frontend', 'enqueued' ) );
        $this->assertFalse( wp_script_is( 'lgpi_cart_js', 'enqueued' ) );

        unset($GLOBALS['pagenow']);
        $this->assertTrue( wp_style_is( 'lgpi_frontend', 'registered' ) );
        $this->assertTrue( wp_script_is( 'lgpi_cart_js', 'registered' ) );
        $this->assertTrue( wp_style_is( 'lgpi_frontend', 'enqueued' ) );
        $this->assertTrue( wp_script_is( 'lgpi_cart_js', 'enqueued' ) );
    }

    /**
     * A single example test.
     */
    public function est_is_request() {
        tests_add_filter( 'init', [$this, 'dummy'] );
        $GLOBALS['pagenow'] = 'wp-login.php';
        $this->assertFalse($this->instance->is_request('frontend'));

        $screen = WP_Screen::get( 'admin_init' );
        $this->assertFalse($this->instance->is_request('frontend'));

        $this->go_to( '/' );
        $this->assertQueryTrue ( 'is_home', 'is_front_page' );
        define( 'DOING_AJAX', true );
        $this->assertTrue($this->instance->is_request('ajax'));

        $this->go_to( 'edit.php' );
        //$this->assertFalse($this->instance->is_request('ajax'));
    }

    public function est_init_checkout_assets_false()
    {
        update_option('lgpi_checkout_asset_url', '');
        $lp = LoopiaPlatform::instance();
        $this->assertFalse($lp->init_checkout_assets());

        update_option('lgpi_checkout_asset_url', self::ASSETS_URL);
        $this->assertTrue($lp->init_checkout_assets());

        update_option('lgpi_checkout_asset_url', 'something stupid');
        $this->assertTrue($lp->init_checkout_assets());
    }

    /**
     * @group summary
     */
    public function test_generate_summary_html()
    {
    	$method = self::getMethod(get_class($this->instance), 'generate_summary_html');
    	$summary['price']['vatExcluded'] = '5';
    	$html = $method->invokeArgs($this->instance, [$summary, 'SEK']);
    	// $this->assertEquals('<div class="lgpi-cart-summary-items"></div>', $html);
    	// self::debug($html);
    	$this->assertEquals('<div class="lgpi-cart-summary-items"><div class="lgpi-cart-summary-item"><span class="lgpi-cart-summary-total-price lgpi_price_inner_container"><span class="lgpi-cart-summary-total-price-vat-excluded lgpi_price_excluding_vat"><span class="lgpi-cart-summary-item-name lgpi-price-vat-excluded">Total price (excluding VAT)</span><span class="lgpi-cart-summary-item-value lgpi-price-vat-excluded-value">5SEK</span></span><span class="lgpi-cart-summary-total-price-vat-included lgpi_price_including_vat"><span class="lgpi-cart-summary-item-name lgpi-price-vat-included">Total price (including VAT)</span><span class="lgpi-cart-summary-item-value lgpi-price-vat-included-value">6.25SEK</span></span></span></div></div>', $html);
    }

}
