<?php

/**
 * Class LoopiaPlatformCartApiClientTest
 */

include_once 'class-LoopiaPlatformBaseTest.php';

class LoopiaPlatformCartApiClientTest extends LoopiaPlatformBaseTest {

    /**
     * @var LoopiaPlatformCartApiClient
     */
    private $instance;
    /**
     * This function will be executed before each test.
     */
    public function setUp(){
        parent::setup();
        self::remove_cookie();
        $this->instance = new LoopiaPlatformCartApiClient(self::API_URL, true);
    }

    public function test_setHeaders_merged()
    {
        $headers = [
            "test : test values"
        ];

        $this->instance->setHeaders($headers);
        $this->assertEquals(4, count($this->instance->getHeaders()));
    }

    public function test_init_request_headers_without_cookie()
    {
        $method = self::getMethod(get_class($this->instance), 'init_request_headers');
        $method->invokeArgs($this->instance, []);
        $this->assertTrue(count($this->instance->getHeaders()) == 3);
    }

    public function test_init_request_headers_with_cookie()
    {
        //Create MOCK for cookie class object
        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;
        $cookieMock->init_with_string(self::COOKIE_STRING);

        //call the protected init_request_headers method
        $method = self::getMethod(get_class($this->instance), 'init_request_headers');
        $method->invokeArgs($this->instance, []);

        //validate headers
        $this->assertCount(4, $this->instance->getHeaders());
    }

    public function test_call_with_correct_method_calls()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['init_request_headers', 'extract_cookie', 'build_response_from_body','do_request'],
            [self::API_URL, true]
        );
        $mockObj->setHeaders([
            'Content-type' => 'application/json; charset=UTF-8',
            'Accept' => 'application/json',
            'X-Timestamp' =>date("c")
        ]);
        $mockObj->expects($this->once())->method('init_request_headers', []);
        $mockObj->expects($this->once())->method('do_request', ['GET','temp',[],5.0]);
        $mockObj->expects($this->once())->method('extract_cookie',[]);
        $mockObj->expects($this->once())->method('build_response_from_body', []);
        $mockObj->call('GET','/temp');
    }

    /**
     * @throws ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test_extract_cookie_cookie_in_header()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, ['Set-Cookie' => self::COOKIE_STRING]),
        ]);

        $handlerStack = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;

        $this->instance->setGuzzleResponseObject($client->request('GET', '/'));
        $method = self::getMethod(get_class($this->instance), 'extract_cookie');
        $method->invokeArgs($this->instance, []);

        $this->assertEquals(self::COOKIE_NAME, $this->instance->cookie->getName());
        $this->assertEquals(self::COOKIE_VALUE, $this->instance->cookie->getValue());
    }

    /**
     * @throws ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test_extract_cookie_cookie_not_in_header()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, ['Content-type' => 'application/json']),
        ]);

        $handlerStack = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;

        $this->instance->setGuzzleResponseObject($client->request('GET', '/'));
        $method = self::getMethod(get_class($this->instance), 'extract_cookie');
        $method->invokeArgs($this->instance, []);

        $this->assertEquals('', $this->instance->cookie->getName());
        $this->assertEquals('', $this->instance->cookie->getValue());
    }

    /**
     * @throws ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test_build_response_from_body_content_type_json()
    {
        $dummyBody = [
            'this' => 'is',
            'dummy' => 'body'
        ];
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(
                200,
                ['Content-type' => 'application/json'],
                json_encode($dummyBody)
            ),
        ]);

        $handlerStack = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;

        $this->instance->setGuzzleResponseObject($client->request('GET', '/'));
        $method = self::getMethod(get_class($this->instance), 'build_response_from_body');
        $response = $method->invokeArgs($this->instance, []);

        $this->assertEquals(['json'=>json_encode($dummyBody)], $response);
    }

    /**
     * @throws ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test_build_response_from_body_content_type_text()
    {
        $dummyBody = "<html><body>Error</body></html>";
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(
                200,
                ['Content-type' => 'text/html; charset=UTF-8'],
                $dummyBody
            ),
        ]);

        $handlerStack = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;

        $this->instance->setGuzzleResponseObject($client->request('GET', '/'));
        $method = self::getMethod(get_class($this->instance), 'build_response_from_body');
        $response = $method->invokeArgs($this->instance, []);

        $this->assertEquals(["error" => LgpiExceptionMessages::UNEXPECTED_RESPONSE], $response);
    }

    /**
     * @throws ReflectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function test_build_response_from_body_content_type_invalid()
    {
        $dummyBody = "<html><body>Error</body></html>";
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(
                200,
                ['Content-type' => 'some thing other than json or text'],
                $dummyBody
            ),
        ]);

        $handlerStack = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $cookieMock = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $cookieMock;

        $this->instance->setGuzzleResponseObject($client->request('GET', '/'));
        $method = self::getMethod(get_class($this->instance), 'build_response_from_body');
        $response = $method->invokeArgs($this->instance, []);

        // $this->assertFalse($response);
        $this->assertEquals(["error" => LgpiExceptionMessages::UNEXPECTED_RESPONSE], $response);
    }

    public function test_get_current_cart()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method('call', ['GET','cart']);
        $mockObj->get_user_details();
    }

    public function test_add_item_to_cart()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method(
            'call',
            [
                'POST',
                'cart/items',
                [
                    "code" => 'code',
                    "properties" => ['prop1','prop2'],
                    "sellType" => 'sellType'
                ]
            ]
        );
        $mockObj->add_item_to_cart('code',['prop1','prop2'], 'sellType');
    }

    public function test_remove_item_from_cart_invalid_item_id()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $res = $mockObj->remove_item_from_cart('');
        $this->assertEquals(["error" => LgpiExceptionMessages::MISSING_ITEM_ID], $res);
    }

    public function test_remove_item_from_cart_valid_item_id()
    {
        $itemId = '123';

        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method(
            'call',
            [
                'DELETE',
                'cart/items/'.$itemId,
            ]
        );

        $mockObj->remove_item_from_cart($itemId);
    }

    public function test_logout()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method(
            'call',
            [
                'GET',
                'logout',
            ]
        );

        $mockObj->logout();
    }

    public function test_get_user_details()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method(
            'call',
            [
                'GET',
                'users',
            ]
        );

        $mockObj->get_user_details();
    }

    public function test_checkout()
    {
        $mockObj = $this->getMock(
            get_class($this->instance),
            ['call'],
            [self::API_URL, true]
        );

        $mockObj->expects($this->once())->method(
            'call',
            [
                'POST',
                'checkout',
            ]
        );

        $mockObj->checkout();
    }

    /**
     * Unskip this method if you wish to test full flow of cart
     * @group integration
     */
    public function test_get_cart_add_item_remove_item()
    {
        $this->markTestSkipped('Skipping integration test.');

        $mockCookie = $this->getMock(get_class($this->instance->cookie), ['set_cookie_on_server']);
        $this->instance->cookie = $mockCookie;
        $resJson = $this->instance->get_current_cart();
        $_COOKIE['cart_session'] = $this->instance->cookie->getValue();
        $resData = json_decode($resJson, true);

        $this->assertEquals(0, count($resData['data']['items']));

        //ADD ITEM
        $resJson = $this->instance->add_item_to_cart('easy-seo','direct', []);
        $resData = json_decode($resJson, true);
        $this->assertEquals(1, count($resData['data']['items']));

        //ADD ITEM
        $resJson = $this->instance->add_item_to_cart('easy-seo','direct', []);
        $resData = json_decode($resJson, true);
        $this->assertEquals(2, count($resData['data']['items']));

        //REMOVE ITEM
        $cookie[] = $this->instance->cookie->getValue();
        $resJson = $this->instance->remove_item_from_cart($resData['data']['items'][0]['id']);
        $resData = json_decode($resJson, true);
        $this->assertEquals(1, count($resData['data']['items']));
    }
}
