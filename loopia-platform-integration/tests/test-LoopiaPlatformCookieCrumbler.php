<?php
include_once 'class-LoopiaPlatformBaseTest.php';

/**
 * Class LoopiaPlatformCookieCrumblerTest
 */
class LoopiaPlatformCookieCrumblerTest extends LoopiaPlatformBaseTest {

    /**
     * @var LoopiaPlatformCookieCrumbler
     */
    private $instance;

    /**
     * This function will be executed before each test.
     */
    public function setUp(){
        parent::setup();
        $this->instance = new LoopiaPlatformCookieCrumbler();
    }

    public function test_init_with_string_return_false()
    {
        $cookieString = '';
        $this->assertFalse($this->instance->init_with_string(null));
        $this->assertFalse($this->instance->init_with_string($cookieString));
    }

    public function test_init_with_string_calls_extract_cookie_info_once()
    {
        $mockObject = $this->getMock(get_class($this->instance), ['extract_cookie_info', 'set_cookie_on_server']);
        $mockObject->expects($this->once())->method('extract_cookie_info', []);
        $mockObject->expects($this->once())->method('set_cookie_on_server', []);

        $cookieString = 'cart_session=ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa; expires=Sun, 12-Mar-2023 13:02:59 GMT; Max-Age=77760000; path=/; domain=.websupport.se';
        $mockObject->init_with_string($cookieString);
    }

    public function test_extract_cookie_info_set_data()
    {
        //With all data except httponly and secure
        $cookieString = 'cart_session=ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa; expires=Sun, 12-Mar-2023 13:02:59 GMT; Max-Age=77760000; path=/; domain=.websupport.se';
        $method = self::getMethod(get_class($this->instance), 'extract_cookie_info');
        $method->invokeArgs($this->instance, [$cookieString]);

        $this->assertTrue('cart_session' == $this->instance->getName());
        $this->assertTrue('ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa'== $this->instance->getValue());
        $this->assertTrue('Sun, 12-Mar-2023 13:02:59 GMT' == $this->instance->getData()['expires']);
        $this->assertTrue($this->instance->getData()['domain'] == '.websupport.se');
        $this->assertFalse($this->instance->getData()['secure']);
        $this->assertFalse($this->instance->getData()['httponly']);


        //With all data including httponly and secure
        $cookieString = 'cart_session=ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa; expires=Sun, 12-Mar-2023 13:02:59 GMT; Max-Age=77760000; path=/; domain=.websupport.se; httponly; secure';
        $method = self::getMethod(get_class($this->instance), 'extract_cookie_info');
        $method->invokeArgs($this->instance, [$cookieString]);
        $this->assertTrue('ekyQvsVvxQZhdzHJytwXiEg0WdOo53clNlHmVuXa'== $this->instance->getValue());
        $this->assertTrue('Sun, 12-Mar-2023 13:02:59 GMT' == $this->instance->getData()['expires']);
        $this->assertTrue($this->instance->getData()['domain'] == '.websupport.se');
        $this->assertTrue($this->instance->getData()['secure']);
        $this->assertTrue($this->instance->getData()['httponly']);
    }
}
